// Находим в документации:  browsersync.io/docs/gulp
const browserSync = require('browser-sync').create();

export const serve = (cb) => {
    browserSync.init({
        server: {
            baseDir: "./",
        },
        port: 2020,
        // Browsersync будет сразу открывать окно браузера:
        open: true,
        // Можем указать браузер в котором будем открывать окно (по стандарту - хром)
        browser: "firefox",
    });
    
    cb();
};
// Экспортируем сервер:
// exports.serve = serve;


