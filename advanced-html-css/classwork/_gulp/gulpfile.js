
// src - метод, который доставляет файлы галпу
// dest - метод, который указывает куда доставляет файлы
// parrallel - метод, который показывает как будут переданы файлы (паралельно или последовательно)
// watch - наблюдает за изменениями внутри файла

const {src, dest, parallel, series, watch} = require("gulp");
const browserSync = require("browser-sync").create();

const serve = (cb) => {
    browserSync.init({
        server: {
            baseDir: "./",
        },
        port: 4400,
        // Browsersync будет сразу открывать окно браузера:
        open: true,
        // Можем указать браузер в котором будем открывать окно (по стандарту - хром)
        // browser: "chrome",
    });
    
    cb();
};

// Как только мы будем видеть изменения в index.html - мы будем перезагружать
// наш браузер: 
// ! Если у нас несолько html файлов - watch('./*.html, (cb) => {});
const watcher = () => {
    watch("./index.html", (cb) => {
        browserSync.reload();
        cb();
    });
    watch("./src/js/*.js", (cb) => {
        js();
        browserSync.reload();
        cb();
    });
};

const build = () => {
    js();
}

// Перенапрявляем обработанные файлы из src => dist
const js = () => {
    return src("./src/js/index.js").pipe(dest("./dist/js/"));
}

// Запускаем параллельно наш сервер, watcher и build:
  exports.default = parallel(serve, watcher, build);
//   exports.serve = serve;


