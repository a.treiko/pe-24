import React, { PureComponent } from 'react';
import './FilmDetails.scss';
import Characters from '../Characters/Characters'

export default class FilmDetails extends PureComponent {

    render() {
        const {film} = this.props
        return (
            <div className='details'>
                <div className='details-episode'>
                   {film.episodeId + ' episode'}
                </div>
                <div className='details-crawl'>
                    {film.openingCrawl}
                </div>
                <div>
                    <Characters film={film}/>
                </div>
            </div>

        )
    }
}


