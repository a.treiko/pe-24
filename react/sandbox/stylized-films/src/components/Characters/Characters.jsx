import React, { PureComponent } from 'react'
import axios from 'axios';
import Load  from "../Load/Load";

export default class Characters extends PureComponent {
    state = {
        characters: [],
        isLoading: true
    }

    componentDidMount() {
        const { film } = this.props;
        axios.all(film.characters.map(c => axios(c)))
        .then(res => this.setState({isLoading: false, characters: res.map(i => i.data)}))
        // .then(res => this.setState({isLoading: true, characters: res.map(i => console.log(i))}))  
    }

    render() {
        const { characters, isLoading } = this.state

        if (isLoading) {
            return <Load />
        }

        // const characterName = characters.map(c => console.log(c))
        const characterName = characters.map(c => c.name).join(', ')

        return characterName;
    }
}
