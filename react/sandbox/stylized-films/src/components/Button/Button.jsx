import React, { Component } from 'react'

export default class Button extends Component {
    render() {
        const { onClick, title } = this.props;
        return (
            <div>
                 <button onClick={onClick}>{title}</button>
            </div>
        )
    }
}
