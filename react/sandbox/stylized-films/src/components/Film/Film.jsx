import React, { Component } from 'react';
import './Film.scss'
import FilmDetails from '../FilmDetails/FilmDetails';
import Button from '../Button/Button';

export default class Film extends Component {
    state = {
        expanded: false
    }

    expandFilm = () => {
        this.setState({expanded: true})
    }


    render() {
        const {film} = this.props;
        const {expanded} = this.state;
        
        return (
            <li>
                <h1 className='film'>
                    {film.name}          
                </h1>
                {!expanded && <Button onClick={this.expandFilm} title='Открыть детали фильма'/>}
                {expanded && <FilmDetails film={film} />}
            </li>
        )
    }
}
