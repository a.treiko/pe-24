import React, { Component } from 'react';
import './App.css';
import axios from 'axios';
import Film from './components/Film/Film';
import Load from './components/Load/Load';


export default class App extends Component {
  state = {
    films: [],
    isLoading: true,
  }

  componentDidMount() {
    axios('https://ajax.test-danit.com/api/swapi/films')
      .then((res) => this.setState({films: res.data, isLoading: false}))
  }

  render() {
    const { films, isLoading } = this.state;

    if (isLoading) {
      return <Load/>
    }
    
    const filmList = films.map(film => <Film film={film} key={film.id}/>)
    // const filmList = films.map(film => <Film film={film} key={film.id}/>)
    
    return (
      <div className='App'>
        <ol className='list'>
          {filmList}
        </ol>
      </div>
    )
  }
}

