import React, { useEffect, useState } from 'react';
import './FilmDetails.scss';
import Characters from '../Characters/Characters'
import { useHistory, useParams } from 'react-router-dom';
import axios from 'axios';
import Load from '../Load/Load';

const FilmDetails = () => {
    const [film, setFilm] = useState(null);
    const [isLoading, setIsLoading] = useState(true);
    const {filmId} = useParams();
    const history = useHistory();
  
    useEffect(() => {
      axios(`https://ajax.test-danit.com/api/swapi/films/${filmId}`)
        .then((res) => {
          setFilm(res.data)
          setIsLoading(false)
        })
    }, [filmId])

    const goToFilms = () => {
        history.push('/films')
    }
    
    if (isLoading) {
      return <Load/>
    }

    return (
        <div className='details'>
            <div className='details-episode'>
               {film.episodeId + ' episode'}
            </div>
            <div className='details-crawl'>
                {film.openingCrawl}
            </div>
            <div>
                <Characters film={film}/>
            </div>
            <div><button onClick={goToFilms}>Go back</button></div>
        </div>

    )
}

export default FilmDetails;


