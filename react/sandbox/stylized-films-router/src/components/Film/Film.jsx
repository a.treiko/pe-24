import React from 'react';
import './Film.scss'
import Button from '../Button/Button';
import { useHistory } from 'react-router-dom';

const Film = (props) => {
    const {film} = props;
    const history = useHistory()

    const showFilmDetails = () => {
        history.push(`/films/${film.id}`)
    }

    console.log(film);
        return (
            <li>
                <h1 className='film'>
                    {film.name}             
                </h1>
                <Button onClick={showFilmDetails} title='Открыть детали фильма'/>
            </li>
        )
}

export default Film;