import React, { useState, useEffect } from 'react'
import axios from 'axios';
import Card from '../Card/Card';
import './List.scss';

function List() {
    const [isLoading, setIsLoading] = useState(true)
    const [goods, setGoods] = useState([])

    useEffect(() => {
        axios.get('/goods.json')
            .then(res => {
                setGoods(res.data)
                setIsLoading(false)
            })
    }, [])

    if (isLoading) {
        return false
    }

    const addToFavorites = (article) => {
        setGoods({goods: goods.map(item => {
            if(article === item.article) {
                item.isFav = true;
                return item
            }
            return item
        })})
        localStorage.setItem('fav', JSON.stringify(goods.filter(i => i.isFav)))
    }

    const removeFromFavorites = (article) => {
        setGoods({goods: goods.map(item => {
            if(article === item.article) {
                item.isFav = false;
                return item
            }
            return item
        })})
        localStorage.setItem('fav', JSON.stringify(goods.filter(i => i.isFav)))
    }

    const cards = goods.map((card) => (
        <Card 
            key={card.article}
            url={card.url} 
            title={card.title}
            price={card.price}
            color={card.color}
            article={card.article}
            isFav={card.isFav}
            removeFromFavorites={removeFromFavorites}
            addToFavorites={addToFavorites}
        />
    ))


    return (
        <ol className='list'>
            {cards}
        </ol>
    )
}

export default List;


