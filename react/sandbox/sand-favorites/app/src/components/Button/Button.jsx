import React from 'react';
import './Button.scss';

function Button({title}) {
    return (
        <div>
            <button className='button'>{title}</button>
        </div>
    )
}

export default Button;
