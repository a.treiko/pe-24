import React from 'react';
import './Card.scss';
import Button from '../Button/Button';
import Icon from '../Icon/Icon';

function Card ({ article, title, price, color, url, isFav, removeFromFavorites, addToFavorites }) {
    return (
        <li className='card' >
            <img 
                src={url} 
                alt='card-img'
                width='220px'>
            </img>
            <div className='body'>
                <h3 className='card__title'>{title}</h3>
                <p className='card__price'>Price: {price}
                    <span className='card__article'>  Article №: {article}</span>
                </p>
                
            </div>
            <div className='card__footer' style={{backgroundColor: color}}>
                <Button title='Add to basket'/>
                <Icon onClick={() => isFav ? removeFromFavorites(article) : addToFavorites(article)} filled={isFav} />
            </div>
        </li>
    )
}
export default Card;
