import React from 'react'
import { Redirect, Switch, Route } from 'react-router-dom'
import FilmDetails from '../components/FilmDetails/FilmDetails'
import Films from '../components/Films/Films'
import Login from '../pages/Login/Login'

export default function AppRoutes({user, setUser}) {
    const isAuth = !!user;

    return (
        <Switch>
            <Redirect exact from='/' to='/films' />
            <Route exact path='/login'>
                <Login setUser={setUser} isAuth={isAuth} />
            </Route>
            <ProtectedRoute exact path='/films' isAuth={isAuth}><Films /></ProtectedRoute>
            <ProtectedRoute exact path='/films/:filmId' isAuth={isAuth}><FilmDetails /></ProtectedRoute>
        </Switch>
    )
}

const ProtectedRoute = ({ exact, path, children, isAuth }) => {
    return <Route exact={exact} path={path} render={() => {
       if (isAuth) {
        return children
    } else {
         return <Redirect to='/login'/>
    }    
 }} />         
}
