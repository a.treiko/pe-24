import React, { useContext } from 'react';
import './Film.scss'
import Button from '../Button/Button';
import { useHistory } from 'react-router-dom';
import ThemeContext from '../../context/ThemeContext';

const Film = (props) => {
    const {film} = props;
    const history = useHistory()
    const themeContext = useContext(ThemeContext)

    const showFilmDetails = () => {
        history.push(`/films/${film.id}`)
    }

        return (
            <li>
                <h1 className='film' style={themeContext.theme}>
                    {film.name} 
                </h1>
                <Button onClick={showFilmDetails} title='Открыть детали фильма'/>
            </li>
        )
}

export default Film;