import React, { useEffect } from 'react';
import axios from 'axios';
import Film from '../Film/Film';
import Load from '../Load/Load';
import { connect } from 'react-redux';

function Films({films, setFilms, isLoading, setIsLoading}) {
    // const [films, setFilms] = useState([]);
    // const [isLoading, setIsLoading] = useState(true)
  
    useEffect(() => {
      if (!films || !films.length) {
        axios('https://ajax.test-danit.com/api/swapi/films')
        .then((res) => {
          setFilms(res.data)
          setIsLoading(false)
        })
      }
      
    }, [])
    
    if (isLoading) {
      return <Load/>
    }
    
    const film = films.map(film => <Film film={film} key={film.id}/>)
    
    return (
      <div className='App'>
        <ol className='list'>
          {film}
        </ol>
      </div>
    )
}

const mapStateToProps = (state) => {
  return {
    films: state.films.data,
    isLoading: state.films.isLoading
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setFilms: (films) => dispatch({type: 'SET_FILMS', payload: films}),
    setIsLoading: (isLoading) => dispatch({type: 'SET_FILMS_LOADING', payload: isLoading})
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Films)