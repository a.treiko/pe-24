import React from 'react'

const Button = (props) => {
    const { onClick, title } = props;
        return (
            <div>
                 <button onClick={onClick}>{title}</button>
            </div>
        )

}

export default Button;