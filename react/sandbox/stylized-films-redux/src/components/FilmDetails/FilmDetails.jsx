import React, { useEffect, useState } from 'react';
import './FilmDetails.scss';
import Characters from '../Characters/Characters'
import { useHistory, useParams } from 'react-router-dom';
import axios from 'axios';
import Load from '../Load/Load';
import { useDispatch, useSelector } from 'react-redux';

const FilmDetails = () => {
    const dispatch = useDispatch()
    const {filmId} = useParams();
    const film = useSelector(state => state.films.data.find(f => f.id === +filmId) || state.filmDetails.data)
    const isLoading = useSelector(state => !film && state.filmDetails.isLoading)
    const history = useHistory();
  
    useEffect(() => {
        if (!film) {
            axios(`https://ajax.test-danit.com/api/swapi/films/${filmId}`)
            .then((res) => {
                dispatch({type: 'SET_FILM_DETAILS', payload: res.data})
                dispatch({type: 'SET_FILM_DETAILS_LOADING', payload: false})
            })
        }
    }, [filmId])

    const goToFilms = () => {
        history.push('/films')
    }
    
    if (isLoading) {
      return <Load/>
    }

    return (
        <div className='details'>
            <div className='details-episode'>
               {film.episodeId + ' episode'}
            </div>
            <div className='details-crawl'>
                {film.openingCrawl}
            </div>
            <div>
                <Characters film={film} />
            </div>
            <div><button onClick={goToFilms}>Go back</button></div>
        </div>

    )
}

export default FilmDetails;


