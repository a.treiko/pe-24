import React, { useEffect, useState } from 'react'
import axios from 'axios';
import Load  from "../Load/Load";

const Characters = (props) => {
    const { film } = props;
    const [characters, setCharacters] = useState([])
    const [isLoading, setIsLoading] = useState(true)

    useEffect(() => {
        axios.all(film.characters.map(c => axios(c)))
            .then(res => {
                setCharacters(res.map(i => i.data))
                setIsLoading(false)
            })
    }, [])

    if (isLoading) {
        return <Load />
    }
    
    // const characterName = characters.map(c => console.log(c))
    const characterName = characters.map(c => c.name).join(', ')

    return characterName;

}

export default Characters;