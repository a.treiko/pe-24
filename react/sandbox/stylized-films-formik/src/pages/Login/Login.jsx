import { withFormik } from 'formik'
import React from 'react'
import { Redirect } from 'react-router-dom'
import LoginInput from './LoginInput'
import schema from './schema'

function Login({ isAuth, handleSubmit }) {

    if (isAuth) {
        return <Redirect to='/' />
    }

    return (
        <form onSubmit={handleSubmit} noValidate>
            <div>
                <LoginInput name='login' type='text' placeholder='Email' />
            </div>
            <div>
                <LoginInput name='password' type='password' placeholder='Password' />
            </div>
            <div>
                <button type='submit'>Log In</button>
            </div>
        </form>
    )
}

const logInUser = (values, {props}) => {     // вторым параметром к нам приходят пропсы компонента, где мы можем найти setUser
   console.log(props);
    props.setUser(values)
}

export default withFormik({
    mapPropsToValues: (props) => ({
        login: '',
        password: '',
    }),
    handleSubmit: logInUser,
    validationSchema: schema
})(Login)
