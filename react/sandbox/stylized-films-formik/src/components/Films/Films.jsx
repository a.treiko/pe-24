import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Film from '../Film/Film';
import Load from '../Load/Load';

export default function Films() {
    const [films, setFilms] = useState([]);
    const [isLoading, setIsLoading] = useState(true)
  
    useEffect(() => {
      axios('https://ajax.test-danit.com/api/swapi/films')
        .then((res) => {
          setFilms(res.data)
          setIsLoading(false)
        })
    }, [])
    
    if (isLoading) {
      return <Load/>
    }
    
    const film = films.map(film => <Film film={film} key={film.id}/>)
    
    return (
      <div className='App'>
        <ol className='list'>
          {film}
        </ol>
      </div>
    )
}
