import React, { useEffect, useState } from 'react';
import './App.css';
import axios from 'axios';
import Film from './components/Film/Film';
import Load from './components/Load/Load';

const App = () => {
  const [films, setFilms] = useState([]);
  const [isLoading, setIsLoading] = useState(true)

  useEffect(() => {
    axios('https://ajax.test-danit.com/api/swapi/films')
      .then((res) => {
        setFilms(res.data)
        setIsLoading(false)
      })
  }, [])
  


  if (isLoading) {
    return <Load/>
  }
  
  const filmList = films.map(film => <Film film={film} key={film.id}/>)
  // const filmList = films.map(film => <Film film={film} key={film.id}/>)
  
  return (
    <div className='App'>
      <ol className='list'>
        {filmList}
      </ol>
    </div>
  )

}

export default App;