import React from 'react';
import './FilmDetails.scss';
import Characters from '../Characters/Characters'

const FilmDetails = (props) => {
    const {film} = props
    return (
        <div className='details'>
            <div className='details-episode'>
               {film.episodeId + ' episode'}
            </div>
            <div className='details-crawl'>
                {film.openingCrawl}
            </div>
            <div>
                <Characters film={film}/>
            </div>
        </div>

    )
}

export default FilmDetails;


