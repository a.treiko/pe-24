import React, { useState } from 'react';
import './Film.scss'
import FilmDetails from '../FilmDetails/FilmDetails';
import Button from '../Button/Button';

const Film = (props) => {
    const {film} = props;
    const [expanded, setExpanded] = useState(false)

    const expandFilm = () => {
        setExpanded(true)
    }
        return (
            <li>
                <h1 className='film'>
                    {film.name}             
                </h1>
                {!expanded && <Button onClick={expandFilm} title='Открыть детали фильма'/>}
                {expanded && <FilmDetails film={film} />}
            </li>
        )
}

export default Film;