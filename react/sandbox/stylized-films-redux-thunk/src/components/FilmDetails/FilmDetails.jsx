import React, { useEffect, useState } from 'react';
import './FilmDetails.scss';
import Characters from '../Characters/Characters'
import { useHistory, useParams } from 'react-router-dom';
import axios from 'axios';
import Load from '../Load/Load';
import { useDispatch, useSelector } from 'react-redux';
import { getFilmDetailsOperation } from '../../store/operations';

const FilmDetails = () => {
    const { filmId } = useParams();
    const dispatch = useDispatch()
    const film = useSelector(state => state.films.data.find(f => f.id === +filmId) || state.filmDetails.data)
    const isLoading = useSelector(state => !film && state.filmDetails.isLoading)

    const history = useHistory();
  
    useEffect(() => {
        if (!film) {
            dispatch(getFilmDetailsOperation(filmId))
        }
    }, [filmId])

    const goToFilms = () => {
        history.push('/films')
    }
    
    if (isLoading) {
      return <Load/>
    }

    return (
        <div className='details'>
            <div className='details-episode'>
               {film.episodeId + ' episode'}
            </div>
            <div className='details-crawl'>
                {film.openingCrawl}
            </div>
            <div>
                <Characters film={film} />
            </div>
            <div><button onClick={goToFilms}>Go back</button></div>
        </div>

    )
}

export default FilmDetails;


