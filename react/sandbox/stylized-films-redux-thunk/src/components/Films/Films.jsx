import React, { useEffect } from 'react';
import Film from '../Film/Film';
import Load from '../Load/Load';
import { connect } from 'react-redux';
import { getFilmsOperation } from '../../store/operations';

function Films({ films, getFilmsOperation, isLoading }) {
    // const [films, setFilms] = useState([]);
    // const [isLoading, setIsLoading] = useState(true)
  
    useEffect(() => {
      if (!films || !films.length) {
        getFilmsOperation()
      }
    }, [])
    
    if (isLoading) {
      return <Load/>
    }
    
    const film = films.map(film => <Film film={film} key={film.id}/>)
    
    return (
      <div className='App'>
        <ol className='list'>
          {film}
        </ol>
      </div>
    )
}

const mapStateToProps = (state) => {
  return {
    films: state.films.data,
    isLoading: state.films.isLoading
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getFilmsOperation: () => dispatch(getFilmsOperation())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Films)