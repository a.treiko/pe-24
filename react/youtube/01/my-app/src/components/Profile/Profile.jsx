import React from 'react';
import s from './Profile.module.css';
import MyPosts from './MyPosts/MyPosts';

const Profile = () => {
    return (
        <div className = {s.content}>
          <div>
            <img className= {s.img} src="https://artnet-studio.ru/wp-content/uploads/2019/01/1GkR93AAlILkmE_3QQf88Ug.png" alt="pic"/>
          </div>
          <div>
            ava + description
          </div>
      <MyPosts />
        </div>
    )
}

export default Profile;