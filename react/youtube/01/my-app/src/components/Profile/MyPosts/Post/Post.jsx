import React from 'react';
import s from './Post.module.css';

const Post = (props) => {
    return (
        <div className = {s.item}>
            <img src="http://udp.by/customlib/htmltemplate/admin/pixit/assets/img/avatars/avatar1.png" alt="avatar"/>
            {props.message}
            <div>
                <span>like</span> {props.likesCount}
            </div>
        </div>
    )
}

export default Post;