import React from 'react';
import s from './MyPosts.module.css';
import Post from './Post/Post';

const MyPosts = () => {
    return (
        <div>
            My posts
            <div> 
              <textarea name="" id="" cols="30" rows="10"></textarea>
              <button>Add Post</button>
              <button>Remove Post</button>
            </div>
            <Post message='Hi, how are you?' likesCount='0'/>
            <Post message='It is my first post' likesCount='23' />

          </div>
    )
}

export default MyPosts;