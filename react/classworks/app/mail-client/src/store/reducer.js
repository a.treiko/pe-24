import { combineReducers } from 'redux';
import user from './user/reducer';
import emails from './emails/reducer'

const reducer = combineReducers({
  user,
  emails
})

export default reducer;