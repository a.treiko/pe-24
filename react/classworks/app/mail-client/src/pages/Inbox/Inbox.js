import React, { useEffect } from 'react';
import Email from '../../components/Email/Email';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import Loader from '../../components/Loader/Loader';
import { getEmailsSelector, emailsLoadingSelector } from '../../store/emails/selectors';
import { getEmailsOperation } from '../../store/emails/operations';


const Inbox = () => {
  const dispatch = useDispatch()
  const emails = useSelector(getEmailsSelector);
  const isLoading = useSelector(emailsLoadingSelector);

  // Например: загрузка с сервера - эффект 
  // в useEffect мы передаем функцию, которая выполняется после каждого рендера
  useEffect(() => {
    // componentDidMount, componentDidUpdate
    dispatch(getEmailsOperation())
  }, [dispatch]);

  if (isLoading) {
    return <Loader />
  }

  const emailCards = emails.map(e => <Email key={e.id} email={e} />);

  return (
    <div>
      <div>
        {emailCards}
      </div>
    </div>
  );
}

Inbox.propTypes = {
  //   emails: PropTypes.arrayOf(PropTypes.shape({
  //   id: PropTypes.number.isRequired,
  //   topic: PropTypes.string.isRequired
  // })).isRequired,
    title: PropTypes.string,
    incrementAge: PropTypes.func,
    updateTitle: PropTypes.func
}

export default Inbox;