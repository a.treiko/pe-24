import React from 'react';
import './App.css';
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
// import Loader from './components/Loader/Loader';
// import axios from 'axios';
import Sidebar from './components/Sidebar/Sidebar';
import AppRouts from './routes/AppRouts';

const App = () =>  {

  return (
    <div className="App">
      <Header />
      <Sidebar />
      <AppRouts />
      <Footer/>
    </div>
  );
}

export default App;

