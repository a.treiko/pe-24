import React, { PureComponent } from 'react'
import Error500 from '../Error500/Error500';
import { withRouter } from 'react-router-dom';

class ErrorBoundary extends PureComponent {
    state = {
        errorPresent: false
    }

    // componentDidCatch(error, errorInfo) {
    //     this.setState({errorPresent: true})
    // }
    static getDerivedStateFromError(error) {
        return {errorPreset: true}
    }

    componentDidUpdate(prevProps)  {
        const { location } = this.props;
        const { errorPresent } = this.state;

        if (errorPresent && location.path !== prevProps.location.path) {
            this.setState({errorPresent: false})
        }
    }

    render() {
        const { errorPresent } = this.state;
        const { children } = this.props;

        // console.log(this);

        if (errorPresent) {
            return <Error500 />
        }

        return children
    }
}

export default withRouter(ErrorBoundary);