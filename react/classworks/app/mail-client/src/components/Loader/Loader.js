import React, { PureComponent } from 'react'

export default class Loader extends PureComponent {
    render() {
        return (
            <div>
                <h2>Loading...</h2>
            </div>
        )
    }
}
