import React from 'react';
import * as icons from '../../theme/icons'

export default function Icon(props) {
    const { type, color, filled, onClick } = props;

    const iconJsx = icons[type];

    if (!iconJsx) {
        return null;
    }

    return (
        <span onClick={onClick}>
            {iconJsx(color, filled)}
        </span>
    )
}
