const express = require('express')

const app = express();

const emails = [
    {id: 1, topic: 'Email 1', favorite: true, body: 'Email - 1 Lorem Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.' },
    {id: 2, topic: 'Email 2', body: 'Email - 2 Lorem Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.”'},
    {id: 3, topic: 'Email 3', body: 'Email - 3 Lorem Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit amet consectetur adipisci[ng] velit, sed quia non numquam [do] eius modi tempora inci[di]dunt, ut labore et dolore magnam aliquam quaerat voluptatem.'},
]

app.get('/api/emails', (req, res) => {
    res.send(emails);
})

app.get('/api/emails/:emailId', (req, res) => {
    const { emailId } = req.params;
    res.send(emails.find(e => e.id === +emailId));
})

const port = 8085;

app.listen(port, () => {
    console.log(`Server listening on port ${port}`);
})



