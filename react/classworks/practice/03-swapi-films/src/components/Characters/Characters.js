import React, { PureComponent } from 'react'
import Loader from '../Loader/Loader';
import axios from 'axios';

export default class Characters extends PureComponent {

    state = {
        characters: [],
        isLoading: true,
    }

    componentDidMount = () => {
        const {film} = this.props;
        axios.all(film.characters.map(c => axios(c)))
            .then(res => this.setState({isLoading: false, characters: res.map( i => i.data)}))
    }

    render() {
        const {film} = this.props;
        const {characters, isLoading} = this.state;

        if (isLoading) {
            return <Loader />
        }

        const characterNames = characters.map(c => c.name).join(', ');

        return characterNames;
        
    }
}
