import React, { Component } from 'react'
import FilmDetails from '../FilmDetails/FilmDetails';

export default class Film extends Component {
    state = {
        expanded: false,
    }

    expandFilm = () => {
        this.setState({expanded: true})
    }

    render() {
        const { expanded } = this.state;
        const { film } = this.props;

        return (
            <li>
                <h1>{film.name}</h1>
                {!expanded && <button onClick={this.expandFilm}>Детальнее</button>}
                {expanded && <FilmDetails film={film}/>}  
            </li>
        )
    }
}
