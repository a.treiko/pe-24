import  { useState } from 'react'
import FilmDetails from '../FilmDetails/FilmDetails';

export default function Film(props) {
    const {film} = props;
    const [expanded, setExpanded] = useState(false);

    const expandFilm = () => {
        setExpanded(true);
    }

    return (
        <li>
            <div>
                {film.name}
                {!expanded && <button onClick={expandFilm}>Детальнее</button>}
                {expanded && <FilmDetails film={film}/>}
            </div>
        </li>
    )     
    
}
 