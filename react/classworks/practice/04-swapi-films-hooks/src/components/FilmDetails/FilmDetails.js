import React from 'react'
import Characters from '../Characters/Characters'

export default function FilmDetails(props) {

    const {film} = props;

    return (
        <div>
            <>
                <div>Episode ID: {film.episodeId}</div>
                <div>Opening Crawl: {film.openingCrawl}</div>
                <div>Characters: <Characters film={film}/></div>
            </>
        </div>
    )
    
}
