import React from 'react'
import { Switch, Redirect, Route } from 'react-router-dom'
import Films from '../components/Films/Films'
import FilmDetails from '../components/FilmDetails/FilmDetails'

export default function AppRoutes() {
    return (
        <Switch>
            <Redirect exact from='/' to='/films' />
            <Route exact path='/films'><Films /></Route>
            <Route exact path='/films/:filmId'><FilmDetails /></Route>
        </Switch>
    )
}
