import React from 'react'

export default function Header({user, setUser}) {
    const isAuth = !!user;

    const logOutUser = () => {
        setUser(null);
    }

    return (
        <div>
            {!isAuth && <div>Welcome, anonimous</div>}
            {isAuth && <div>{user.login}</div>}
            {isAuth && <button onClick={logOutUser}>Log out</button>}
        </div>
    )
}


