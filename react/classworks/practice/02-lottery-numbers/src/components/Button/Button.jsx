import React, {PureComponent} from 'react';

class Button extends PureComponent {
    render() {
        const {title, onClick} = this.props;

        return (
            <button onClick={onClick}>{title}</button>
        )
    }
}

export default Button;