// import Films from './components/Films/Films';
import AppRoutes from './routes/AppRoutes';
import Header from './components/Header/Header';
import { useState } from 'react';

const App = () => {
  const [user, setUser] = useState(JSON.parse(localStorage.getItem('user')));

  // useEffect(() => {
  //   setUser(JSON.parse(localStorage.getItem('user')))
  // }, [])

  const setAndStoreUser = (user) => {
    setUser(user);
    localStorage.setItem('user', JSON.stringify(user));
  }

  return (
    <>
      <Header user={user} setUser={setAndStoreUser}/>
      <AppRoutes user={user} setUser={setAndStoreUser}/>
    </>
  )
}

export default App;
