import React, { useState, useEffect } from 'react'
import Characters from '../Characters/Characters'
import { useParams, useHistory } from 'react-router-dom';
import Loader from '../Loader/Loader';
import axios from 'axios';

export default function FilmDetails() {
    const [film, setFilm] = useState(null);
    const [isLoading, setIsLoading] = useState(true);
    const { filmId } = useParams();
    const history = useHistory();
  

    useEffect(() => {
      axios(`https://ajax.test-danit.com/api/swapi/films/${filmId}`)
        .then(res => {
          setFilm(res.data)
          setIsLoading(false)
        })
    }, [filmId])

    const goToFilms = () => {
        history.push('/films')
    }
  
    if(isLoading) {
      return <Loader />
    }

    return (
        <div>
            <>
                <div>Episode ID: {film.episodeId}</div>
                <div>Opening Crawl: {film.openingCrawl}</div>
                <div>Characters: <Characters film={film}/></div>
                <div><button onClick={goToFilms}>Go back</button></div>
            </>
        </div>
    )
    
}
