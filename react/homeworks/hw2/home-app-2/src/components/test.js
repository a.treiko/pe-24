import React, {useState, useEffect} from 'react';

export default function fav(){
    const [data, setData] = useState(null)
    useEffect(() => {
        setData(JSON.parse(localStorage.getItem('fav')))
    }, [])
}