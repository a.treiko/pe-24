import React, { Component } from 'react'
import './Button.scss'

export default class Button extends Component {
    render() {
        const { title, onClick, bgc, padding, hideModal } = this.props;

        return (
            <div>
                <button onClick={onClick} className='button' style={{backgroundColor: bgc, padding: padding}} hideModal={hideModal} >{ title }</button>
            </div>
        )
    }
}
