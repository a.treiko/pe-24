import { Component } from 'react';
import './List.scss';
import axios from 'axios';
import Card from '../Card/Card';
import PropTypes from 'prop-types';

class List extends Component {

    state = {
        goods: [],
        isLoading: true,
    }

    componentDidMount() {
        axios.get('/goods.json')
            .then((res) => {
                this.setState({ goods: res.data, isLoading: false });
            })
    }
   
    render() {
        const { goods, isLoading } = this.state;

        if (isLoading) {
            return false
        }

        const addToFav = (article) => {
            this.setState({goods: goods.map(item => {
                if(article === item.article) {
                    item.isFav = true;
                    return item
                }
                return item
            })})
            localStorage.setItem('fav', JSON.stringify(this.state.goods.filter(i => i.isFav)))
        };
        
        const removeFromFav = (article) => {
            this.setState({goods: goods.map(item => {
                if (article === item.article) {
                    item.isFav = false;
                    return item
                }
                return item
            })})
            localStorage.setItem('fav', JSON.stringify(this.state.goods.filter(i => i.isFav)))
        };

        const items = goods.map((item) => (
            <Card 
                key={ item.article } 
                title={ item.title } 
                price={ item.price } 
                url={ item.url } 
                color={ item.color } 
                article={ item.article } 
                removeFromFav={removeFromFav}
                addToFav={addToFav}
                isFav={item.isFav}
            />
        ))

        return (
            <ol className='list'>
                { items }
            </ol>
        )
    }
}

List.propTypes = {
    goods: PropTypes.array,
    isLoading: PropTypes.bool,
    items: PropTypes.array
}

export default List;