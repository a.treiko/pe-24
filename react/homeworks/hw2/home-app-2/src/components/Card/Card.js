import React, { Component } from 'react'
import './Card.scss'
import Button from '../Button/Button';
import Icon from '../Icon/Icon';
import Modal from '../../Modal/Modal';

class Card extends Component {
    state = {
        isOpen: false,
    }

    toggleModal = () => {
        this.setState({isOpen: !this.state.isOpen})
    }

    openModal =() => {
        this.setState({isOpen: true})
    }

    hideModal = () => {
        this.setState({isOpen: false})
    }

    modalInfo = {
        closeButton: true,
        header: 'Do you want to buy this seed?',
        text: 'Shipping will be free?!',
        actions: <><Button title='Ok' padding='10px 40px'/><Button title='Cancel' onClick={this.toggleModal} padding='10px 40px'/></>
    }




    render() {
        const { isOpen } = this.state;
        const { title, price, url, color, article, addToFav, removeFromFav, isFav } = this.props;

        return (
            <li className='card'>
                <img
                    src={ url }
                    alt={`item-pic-${ article }`}
                    className='card-image'
                    width='200px'>
                </img>
                <div className='card-body'>
                    <h2 className='card-title'>{ title }</h2>
                    <div className='card-price'>{ price }</div>
                    <div className='card-color'>Color: 
                        <div className='card-square' style={{ backgroundColor: color }}></div>
                        <div className='card-article'>#{article}</div>
                    </div>
                </div>
                <div className='card-footer'>
                    <Button title='Add to basket' onClick={e => {this.openModal()}}/>
                    <Icon onClick={() => isFav ? removeFromFav(article) : addToFav(article)} filled={isFav} />
                    {isOpen && (
                        <Modal onClick={this.toggleModal} modalInfo={this.modalInfo} hideModal={this.hideModal} />
                    )}
                </div>
            </li>
        )
    }
}

export default Card;

