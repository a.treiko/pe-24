import { PureComponent } from 'react';
import './Header.scss';

class Header extends PureComponent {
    render() {
        return (
            <header className='header'>
                <div className='header-container'>
                    <img 
                        src='https://bipbap.ru/wp-content/uploads/2019/06/34230-640x678.jpg'
                        alt='logo'
                        className='header-logo'>
                    </img>
                    <div className='header-basket'>basket</div>
                </div>
                <img 
                    src='https://images.squarespace-cdn.com/content/v1/5a0e11f432601e3cca221064/1569945289447-ED3LS7H7WEA35QL9MMCR/ke17ZwdGBToddI8pDm48kLACC5zXUsNM05_bg5hjHBsUqsxRUqqbr1mOJYKfIPR7LoDQ9mXPOjoJoqy81S2I8N_N4V1vUb5AoIIIbLZhVYxCRW4BPu10St3TBAUQYVKc8LY8PAsIrTIpsKCcvB-8S8A4APw5wyoAL-ZgMnoI_9C5BmbNGkrfo9f9CZkTRRrx/ncf2020-CANNATANK.png'
                    alt='header-image'
                    className='header-image'>
                </img>
                <h1 className='header-title'>Cannabis seeds collections</h1>
            </header>
        )
    }
}

export default Header;
