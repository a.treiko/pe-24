import React, { Component } from "react";
import './App.css';
import Header from './components/Header/Header'
import List from "./components/List/List";

class App extends Component {

  render() {
    return (
      <>
        <Header />
        <List />
      </>
    );
  }

}

export default App;
