import React, { Component } from 'react'
import './Modal.scss'
import Button from '../components/Button/Button';
import PropTypes from 'prop-types';


export default class Modal extends Component {

    componentDidMount(){
        document.addEventListener("click",this.handleOutsideClick);
      }
    //   componentWillUnmount(){
    //     document.removeEventListener("click",this.handleOutsideClick);
    //   }    

    handleOutsideClick = (e) => {
        const { hideModal } = this.props;
        if (e.target.className === 'modal') {
          hideModal();
        }
    }

    render() {

        const { onClick, modalInfo } = this.props;
        const { closeButton , header, text, actions } = modalInfo;

        return (
            <div className='modal'>
                <div className='modal__inner' onClick={e => e.stopPropagation()}>
                    <div className='modal__head'>
                        <h3 className='modal__title'>{header}</h3>
                        {closeButton && <Button onClick={onClick} title='x' bgc='transparent' /> }
                    </div>
                    <div className='modal__body'>
                        {text}
                    </div>
                    <div className='modal__footer'>
                        {actions}
                    </div>
                </div>
            </div>
        )
    }
}

Modal.propTypes = {
    modalInfo: PropTypes.object,
    onClick: PropTypes.func,

}
