import React, {PureComponent} from 'react';
import './Button.css';


class Button extends PureComponent {

    render() {
        const {title, bgc, onClick, padding, margin} = this.props;
        return (
            <div className='wrap'>
                <button onClick={onClick} className='btn' style={{backgroundColor: bgc, padding: padding, margin: margin}}>{title}</button>
            </div>
        )
    }
}

export default Button;
