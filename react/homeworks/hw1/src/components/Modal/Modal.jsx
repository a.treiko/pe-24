import React, { Component } from 'react'
import './Modal.scss'
import Button from '../Button/Button';


export default class Modal extends Component {

    handleOutsideClick = (e) => {
        const {hideModal} = this.props;
        if (e.target.className === 'modal') {
          hideModal();
        }
    }
    render() {
        const {onClick, modalInfo} = this.props;
        const {header, closeButton, text, question} = modalInfo;

        return (
            <div className='modal' onClick={onClick}>
                <div className='modal-inner' onClick={e => e.stopPropagation()}>
                    <div className='modal-head'>
                        <h3 className='modal-title'>{header}</h3>
                        {closeButton && <Button onClick={onClick} bgc='transparent' title='x'/>}
                    </div>
                    <div className='modal-body'>
                        <div className='modal-text'>{text}</div>
                        <div className='modal-text'>{question}</div>
                    </div>
                    <div className='modal-footer'>
                        <Button bgc='hsla(0, 0%, 0%, 0.100)' title='Ok' padding='1rem 3rem' margin='1rem' onClick={onClick}></Button>
                        <Button bgc='hsla(0, 0%, 0%, 0.100)' title='Close' padding='1rem 3rem' onClick={onClick}></Button>
                    </div>
                </div>
            </div>
        )
    }
}
