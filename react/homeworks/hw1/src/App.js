import React, {Component} from 'react';
import './App.css';
import Button from './components/Button/Button';
import Modal from './components/Modal/Modal';


class App extends Component {

  state = {
    isOpen: false,
    modal: '',
  }

  modalInfo = {
    first: {
      header: 'Do you want to delete this file?',
      closeButton: true,
      text: `Once you delete this file, it won't be possible to undo this action`,
      question: `Are you sure you want to delete it?`,
    },
    second: {
      header: 'Do you want to save this file?',
      closeButton: true,
      text: `It'll be saved`,
      question: `Are you sure?`,
    }
  }

  toggleModal = (type = '') => {
    this.setState({isOpen: !this.state.isOpen, modal: type})
  }

  hideModal = () => {
    this.setState({isOpen:false});
  }

  render() {
    const { isOpen } = this.state;

    return (
      <div className="App">
        <Button title='Open first modal' bgc='#0DD95D' onClick={e => {this.toggleModal('first')}} margin='2rem 1rem' padding='1rem 3rem'/>
        <Button title='Open second modal' bgc='#8318D9' onClick={e => {this.toggleModal('second')}} margin='2rem 1rem' padding='1rem 3rem'/>
        {isOpen && <Modal onClick={this.toggleModal} modalInfo={this.modalInfo[this.state.modal]} hideModal={this.hideModal}/>}
      </div>
    );
  }
}

export default App;
