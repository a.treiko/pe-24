import React from 'react'
import styled from 'styled-components';

const StyledTitle = styled.h1`
    font-size: 2rem;
    text-align: center;
    color: ${props => props.color || props.theme.colors.primary};
`

const Title = (props) => {
    return (
        <StyledTitle {...props}/>
    )
}  

export default Title
