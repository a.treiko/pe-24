import React from 'react'
import styled, { css, keyframes } from 'styled-components'

const rotateAnimation = keyframes`
    0% {
        transform: rotateZ(0deg)
    }
    100% {
        transform: rotateZ(360deg)
    }
`;

const StyledButton = styled.button`
    border: none;
    padding: 10px 15px;
    font-size: 1.1rem;
    cursor: pointer;
    &:focus {
        outline: none;
    };
    &:hover {
        animation: ${rotateAnimation} 1s infinite linear;
    };
    align-self: ${props => props.align || 'stretch'};

    ${props => props.primary && css`
        color: ${props => props.color || '#FFFFFF'};
        background: ${props => props.background || '#FFFFFF'};
    `}
    ${props => props.outlined && css`
        color: ${props => props.color || '#FFFFFF'};
        border: 1px solid ${props => props.color || '#FFFFFF'};
        background: transparent;
    `}
    
`;

const LargeButton = styled(StyledButton)`
    font-size: 2rem;
`

const Button = (props) => {
    return <LargeButton {...props}/>
}

export default Button
