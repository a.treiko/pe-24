// Подключаем фреймворк
const express = require('express');
// Создаем конфигурационный объект нашего приложения:
const app = express();

const emails = [
    { id: 1,  topic: 'daria.babii@gmail.com', favorite: true, body: 'Софи́я (Софи́) Магдале́на Шолль (нем. Sophia (Sophie) Magdalena Scholl; 9 мая 1921, Форхтенберг, Веймарская республика — 22 февраля 1943, Мюнхен, Третий рейх) — немецкая студентка и активистка движения Сопротивления. Вместе со своим старшим братом Гансом и несколькими другими студентами Мюнхенского университета она состояла в группе «Белая роза», участники которой проводили мирные антинацистские акции, в основном распространение листовок и рисование граффити. Софи и Ганс были арестованы гестапо 18 февраля 1943 года по обвинению в государственной измене и уже 22 февраля казнены на гильотине.' },
    { id: 2,  topic: 'bim.bom@gmail.com', body: 'Перл Бак, урождённая Са́йденстрикер (англ. Pearl Sydenstricker Buck; китайское имя кит. 赛珍珠, пиньинь Sài Zhēnzhū, палл. Сай Чжэньчжу; 26 июня 1892 — 6 марта 1973) — американская писательница и переводчица. Действие её произведений, преимущественно, разворачивается в Китае; перевела на английский язык классический китайский роман «Речные заводи» (1933). Также известна биографической прозой.' },
    { id: 3,  topic: 'papa.carlo@gmail.com', body: 'В отличие от традиционных энциклопедий, таких, как Encyclopædia Britannica, ни одна статья в Википедии не проходит формального процесса экспертной оценки. Любая статья Википедии может редактироваться как с учётной записи участника, так даже и без регистрации на проекте (за исключением некоторых страниц, подверженных частому вандализму, которые доступны для изменения только определённым категориям участников или, в крайних случаях, только администраторам Википедии), и при этом все внесённые в статью изменения незамедлительно становятся доступными для просмотра любыми пользователями.' },
  ]

// Создаем адрес на сервере, который будет иметь этот список эмейлов:
// Вторым параметром записываем функцию, которая говорит, что он будет делать
// Функция имеет в параметрах два объекта request и response
// request - то что пришло с фронтенда
// response - то что мы отправляем на фронтенд
// res.send(emails) - в ответе отправляем список эмейлов
app.get('/api/emails', (req, res) => {
    res.send(emails)
})

app.get('/api/emails/:emailId', (req, res) => {
    const { emailId } = req.params
    res.send(emails.find(e => e.id === +emailId))
})

// Вводим порт:
const port = 3030;
// Запускаем сервер на порту, который мы указали:
app.listen(port, () => {
    console.log(`Server listening on port ${port}`)
})
    //START: node ./index.js

// http://localhost:3030/api/emails - раздает массив эмейлов




// const express = require('express')

// const app = express();

// const emails = [
//   { id: 1, topic: 'Email 1', favorite: true, body: 'Email 1 - Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов.' },
//   { id: 2, topic: 'Email 2', body: 'Email 2 - Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов.' },
//   { id: 3, topic: 'Email 3', body: 'Email 3 - Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов.' }
// ]

// app.get('/api/emails', (req, res) => {
//   res.send(emails);
// })

// app.get('/api/emails/:emailId', (req, res) => {
//   const { emailId } = req.params
//   res.send(emails.find(e => e.id === +emailId));
// })

// const port = 3030;

// app.listen(port, () => {
//   console.log(`Server listening on port ${port}`)
// })
