const initialState = {
    name: 'Artem',
    age: 28
  }

const reducer = (state = initialState, action) => {
    switch(action.type) {
      case "INCREMENT_AGE": {
        return {...state, age: state.age + 1}
      }
      default: {
        return state;
      }
    }
}
export default reducer;