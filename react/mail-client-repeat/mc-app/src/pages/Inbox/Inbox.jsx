import React from 'react'
import './Inbox.scss'; 
import Email from '../../components/Email/Email'
import PropTypes from 'prop-types';

function Inbox (props){
    const { title, emails } = props
        const emailCards = emails.map(email => <Email key={email.id} email={email}/>)

        return (
            <div className='inbox'>
                <div className='container'>
                    <div>{title}</div>
                    <div>{emailCards}</div>
                </div>
            </div>
            )
}

Inbox.propTypes = {
    email: PropTypes.shape({
        id: PropTypes.number.isRequired,
        topic: PropTypes.string.isRequired
    })
}

export default Inbox;
