import React from 'react'
import './Page404.scss'
import { useHistory } from 'react-router-dom'

export default function Page404() {
    const history = useHistory()

    const goToHomePage = () => {
        history.push('/')
    }
    return (
        <div className='page-404'>
            <h2>Page404</h2>
            <h4>Page not found</h4>
            <div><button onClick={goToHomePage}>Go to home page</button></div>
        </div>
    )
}
