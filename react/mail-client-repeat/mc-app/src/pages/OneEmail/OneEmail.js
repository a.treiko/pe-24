import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useParams } from 'react-router-dom';
import ReactLoading from 'react-loading';
import Email from '../../components/Email/Email';

// function OneEmail({history, location, match}) {
    // const emailId = match.params.emailId; 
function OneEmail() {
    const [email, setEmail] = useState(null);
    const [isLoading, setIsLoading] = useState(true)
    const { emailId } = useParams();

    useEffect(() => {
        setIsLoading(true)
        axios(`/api/emails/${emailId}`)
            .then(res => {
                setEmail(res.data)
                setIsLoading(false)
            })
    }, [emailId]) 

    if (isLoading) {
        return <ReactLoading />
    }
    
    return (
        <div>
            <Email email={email} showFull />
        </div>
    )
}

export default OneEmail; 