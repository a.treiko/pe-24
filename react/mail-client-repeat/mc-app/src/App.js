import React, { useState, useEffect } from 'react';
import './App.css';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import ReactLoading from 'react-loading';
import axios from 'axios';
import Sidebar from './components/Sidebar/Sidebar';
import AppRoutes from './routes/AppRoutes';

const App = () => {
  const [user, setUser] = useState(null)
  const [emails, setEmails] = useState([])
  const [isLoading, setIsLoading] = useState(true)

  const getEmails = () => {
    // Вставляем ссылку браузера, на которой наши эмейлы
    // в  then  получаем наш response
    // Добавляем в наш state, где мы указали пустой массив []
    axios.get('/api/emails')
      .then(res => {
        setEmails(res.data)
        setIsLoading(false)
      })
  }

  useEffect(() => {
    getEmails()
  },[])

    return ( 
      <div className='App'> 
        <Header user={user} />
        <Sidebar />
        {isLoading && <ReactLoading type="spin" color="#BFAEB6" height='10' width='10' />}
        {!isLoading && <AppRoutes emails={emails} setUser={setUser} user={user} />}
        <Footer />
      </div>
    )
}
export default App;
