import React from 'react';
import './Footer.scss'
import { connect } from 'react-redux';
import { incrementAgeAction } from '../../store/actions'

function Footer (props){ 
    const { name, age, incrementAge } = props;

    return (
        <div>
            <h2>Footer</h2>
            <div>{name}</div>
            <div>{age}</div>
            <div><button onClick={incrementAge}>Increment Age</button></div>
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        name: state.name,
        age: state.age
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        incrementAge: () => dispatch(incrementAgeAction())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Footer);
