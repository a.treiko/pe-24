import React, { useState } from 'react'
import './Email.scss'
import Icon from '../Icon/Icon';
import { useHistory } from 'react-router-dom';

const Email = (props) => {
    const { email, showFull } = props;
    const [color, setColor] = useState('gold')
    const history = useHistory();
    
    const changeColor = () => {
        setColor('black')
    }

    const goToPrevious = () => {
        history.push(`/emails/${email.id - 1}`)
    }

    const goToNext = () => {
        history.push(`/emails/${email.id + 1}`)
    }
        return (
            <div>
                <div className='email'>
                    {email.topic}
                    <Icon color={color} onClick={changeColor} filled />
                </div>
                { showFull && <div className='email__body'>{email.body}</div> }
                {
                     showFull && (
                        <div className='email__controls'>
                            <button onClick={goToPrevious}>Previous</button>
                            <button onClick={goToNext}>Next</button> 
                        </div>
                    )
                }
            </div>
        )
}

export default Email;