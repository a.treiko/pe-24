import React, { PureComponent } from 'react';
import './Header.scss';
import PropTypes, { func } from 'prop-types';

function Header (props) {
    const { user } = props;

    return (
        <header className='header'>
            <div className='container header__inner'>
                <div>LOGO</div>
                <div className='header__user-info'>
                    {!!user && <div>{user.login}</div>}
                </div>
                <div>MENU</div>
            </div>
        </header>
    )
}

Header.propTypes = {
    title: PropTypes.string,
    user: PropTypes.object
}

export default Header;
// Header.defaultProps = {
//     title: 'OFFLINE'
// }

