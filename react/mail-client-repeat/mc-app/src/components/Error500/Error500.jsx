import React, { Component } from 'react'
import './Error500.scss'
import deadPika from './dead-pikachu.png'

export default class Error500 extends Component {
    render() {
        return (
            <div className='error-500'>
                <h3>An error has occurred</h3>
                <div><img src={deadPika} alt="error-image-500"/></div>
                <h4>But we already try to fix it</h4>
            </div>
        )
    }
}
