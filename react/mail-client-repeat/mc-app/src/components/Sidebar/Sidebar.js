import React from 'react';
import './Sidebar.scss'
import { NavLink } from 'react-router-dom';

export default function Sidebar() {
    return (
        <div>
            <div className='sidebar'>
                <div><NavLink exact className='link' activeClassName='link__active' to='/inbox'>Inbox</NavLink></div>
                <div><NavLink exact className='link' activeClassName='link__active' to='/favorites'>Favorites</NavLink></div>
                <div><NavLink exact className='link' activeClassName='link__active' to='/sent'>Sent</NavLink></div>
                <div><NavLink exact className='link' activeClassName='link__active' to='/login'>Login</NavLink></div>
            </div>
        </div>
    )
}
