import React, { Component } from 'react'
import Error500 from '../Error500/Error500';

export default class ErrorBoundary extends Component {
    state = {
        errorPresent: false
    }
    componentDidCatch(error, errorInfo) {
        this.setState({errorPresent: true})
    }

    // static getDerivedStateFromError(error) {
    //     return {errorPresent: true}
    // }
    
    render() {
        const { errorPresent } = this.state;
        const { children } = this.props;
        if (errorPresent) {
            return <Error500 />
        }
        return children
    }
}
