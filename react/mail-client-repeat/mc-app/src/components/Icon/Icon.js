import React from 'react'
import star from '../../theme/icons/star'

export default function Icon(props) {
    const {color, filled, onClick} = props;
    return (
        <div onClick={onClick}>
            {star(color, filled)}
        </div>
    )
}
