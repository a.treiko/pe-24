import React from 'react'
import Inbox from '../pages/Inbox/Inbox'
import Favorites from '../pages/Favorites/Favorites'
import Sent from '../pages/Sent/Sent'
import { Redirect, Route, Switch } from 'react-router-dom';
import Page404 from '../pages/Page404/Page404';
import OneEmail from '../pages/OneEmail/OneEmail';
import Login from '../pages/Login/Login';


export default function AppRoutes({emails, setUser, user}) {
    const isAuth = !!user;

    return (
        <Switch>
            <Redirect exact from='/' to='/inbox' />
            <Route exact path='/login'><Login setUser={setUser} isAuth={isAuth}/></Route>
            <ProtectedRoute exact path='/inbox' isAuth={isAuth}>
                <Inbox emails={emails} />
            </ProtectedRoute>
            <ProtectedRoute exact path='/favorites' isAuth={isAuth}><Favorites /></ProtectedRoute>
            <ProtectedRoute exact path='/sent' isAuth={isAuth}><Sent /></ProtectedRoute>
            <ProtectedRoute exact path='/emails/:emailId' component={OneEmail} isAuth={isAuth} />
            <Route exact path='*'><Page404 /></Route>
        </Switch>
    )
}

const ProtectedRoute = ({ exact, path, children, isAuth }) => {
    return <Route exact={exact} path={path} render={() => {
       if (isAuth) {
        return children
    } else {
         return <Redirect to='login'/>
    }    
 }} />         
}
 