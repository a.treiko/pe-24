import React, { useCallback, useMemo, useReducer, useState} from 'react';
import './App.css';
import Body from './components/Body/Body';
import Button from './components/Button/Button';
import Footer from './components/Footer/Footer';
import Header from './components/Header/Header';
import reducer from './reducers/emailReducers'
import ThemeProvider from './providers/ThemeProvider'

const emails = [
  {id: 1, topic: 'Email 1', body: 'Email 1 text in body'},
  {id: 2, topic: 'Email 2', body: 'Email 2 text in body'},
  {id: 3, topic: 'Email 3', body: 'Email 3 text in body'},
]

function App() {
  const [state, dispatch] = useReducer(reducer, []) // где [] и есть наш initialState

  const fetchEmails = () => {
    dispatch({type: 'SET_EMAILS', payload: emails})
  }

  // useEffect(() => {
  //   fetchEmails()
  // }, [])

  const emailCards = emails.map(e => <div key={e.id}>{e.topic}</div>)

  const numbers = useMemo(() => [1, 2, 3], [])

  return (
    <div className="App">
      {emailCards}
      <Button title='Click me' />
      <ThemeProvider>
        <Header />
        <Body title='Hello world' numbers={numbers} fetchEmails={fetchEmails} />
        <Footer />
      </ThemeProvider>
    </div>
  );
}

export default App;
