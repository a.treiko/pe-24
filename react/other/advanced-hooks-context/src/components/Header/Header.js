import React, { memo, useContext } from 'react'
import ThemeContext from '../../context/themeContext'
import Button from '../Button/Button'

 function Header() {
    const context = useContext(ThemeContext);

    return (
        <div>
            <h2>Header</h2>
            <Button title='Button in Header' onClick={context.toggleTheme}/>
        </div>
    )
}
export default memo(Header)
