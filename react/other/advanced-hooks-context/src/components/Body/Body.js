import React, { memo } from 'react'

function Body({title, numbers}) {
    return (
        <div>
            <h2>Body</h2>
            <p>{title}</p>
            <p>{numbers}</p>
        </div>
    )
}

export default memo(Body)