import React, { useContext } from 'react'
import ThemeContext from '../../context/themeContext'

const Button = ({title, onClick}) => {
    const context = useContext(ThemeContext)
    return (
        <div>
            <button style={context.theme} onClick={onClick}>{title}</button>
        </div>
    )
}

export default Button
