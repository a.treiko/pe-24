import React from 'react'

export const themes = {
    light: {
        color: '#000',
        background: '#fff'
    },
    dark: {
        color: '#fff',
        background: '#222'
    }
}

// Создаем объект контекста:
// Где мы указываем значение по умолчанию theme.dark
const ThemeContext = React.createContext({theme: themes.dark});
ThemeContext.displayName = 'Theme Context'
export default ThemeContext

