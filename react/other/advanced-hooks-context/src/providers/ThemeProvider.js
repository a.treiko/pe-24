import React, { useCallback, useState } from 'react'
import ThemeContext, {themes} from '../context/themeContext';

const ThemeProvider = ({children}) => {
    const [theme, setTheme] = useState(themes.light);

    const toggleTheme = useCallback(() => {
        setTheme(theme === themes.dark ? themes.light : themes.dark)
        },[theme])

    return (
        <ThemeContext.Provider value={{theme: theme, toggleTheme: toggleTheme}}>
            {children}
        </ThemeContext.Provider>
    )
}

export default ThemeProvider
