import './App.css';
import UncontrolledForm from './components/UncontrtolledForm/UncontrolledFrom';
import ControlledForm from './components/ControlledForm/ControlledFrom'
import FormikForm from './components/FormikForm/FormikForm';
import FormikFormShort from './components/FormikFormShort/FormikFormShort';
import FormikFormShort2 from './components/FormikFormShort2/FormikFormShort2';
import FormikYup from './components/FormikYup/FormikYup';

function App() {
  return (
    <div className="App">
      <UncontrolledForm />
      <ControlledForm />
      <FormikForm />
      <FormikFormShort />
      <FormikFormShort2 />
      <FormikYup />
    </div>
  );
}

export default App;
