import React from 'react';
import { withFormik } from 'formik'; 
import MyInput2 from './MyInput2';

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

const validateForm = (values) => {
    const { login, password, repeatPassword } = values
    const errors = {} 

    if (!EMAIL_REGEX.test(login)) {
      errors.login = 'This is not a valid email'
    }

    if (!login) {
      errors.login = 'This field is required'
    }

    if (password !== repeatPassword) {
      errors.repeatPassword = 'Passwords do not match'
    }

    if (password.length < 8) {
      errors.password = 'Password should contain at least 8 characters'
    }

    if (!password) {
      errors.password = 'This field is required'
    }

    if (!repeatPassword) {
      errors.repeatPassword = 'This field is required'
    }

    return errors;
  }

function FormikFormShort2(props) {
    return (
        <form onSubmit={props.handleSubmit} noValidate>
            <h3>FormikFormShort2</h3>
            <MyInput2 name='login' type='email' placeholder='Email' />
            <MyInput2 name='password' type='password' placeholder='Password' />
            <MyInput2 name='repeatPassword' type='password' placeholder='Repeat Password' />                       
            <div>
                <button type='submit' disabled={props.isSubmitting}>Register</button>
            </div>
        </form>)
    
}

const register = (values, helpers) => { // (values, { setSubmitting }) => setSubmitting(false)
  console.log(values, helpers);
  helpers.setSubmitting(false)
}

export default withFormik({
    mapPropsToValues: (props) => ({
        login: 'admin@test.com',
        password: '',
        repeatPassword: ''
    }),
    handleSubmit: register,
    validate: validateForm
})(FormikFormShort2)