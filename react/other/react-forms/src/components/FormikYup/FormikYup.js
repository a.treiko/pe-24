import React from 'react'
import { withFormik } from 'formik';
import schema from './schema';
import FormikYupInput from './FormikYupInput';

function FormikForm2(props) {
  return (
    <form onSubmit={props.handleSubmit} noValidate>
      <h3>FormikYup</h3>
      <FormikYupInput name='login' type='email' placeholder='Email' />
      <FormikYupInput name='password' type='password' placeholder='Password' />
      <FormikYupInput name='repeatPassword' type='password' placeholder='Repeat Password' />
      <div>
        <button type='submit' disabled={props.isSubmitting}>Register</button>
      </div>
    </form>
  )
}

const register = (values, { setSubmitting }) => {
  console.log(values);
  setSubmitting(false);
}

export default withFormik({
  mapPropsToValues: (props) => ({
    login: 'admin@test.com',
    password: '',
    repeatPassword: ''
  }),
  handleSubmit: register,
  validationSchema: schema
})(FormikForm2)
