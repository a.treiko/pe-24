import React from 'react';
import { Formik, Form, Field } from 'formik'; 

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

const validateForm = (values) => {
    const { login, password, repeatPassword } = values
    const errors = {}

    if (!EMAIL_REGEX.test(login)) {
      errors.login = 'This is not a valid email'
    }

    if (!login) {
      errors.login = 'This field is required'
    }

    if (password !== repeatPassword) {
      errors.repeatPassword = 'Passwords do not match'
    }

    if (password.length < 8) {
      errors.password = 'Password should contain at least 8 characters'
    }

    if (!password) {
      errors.password = 'This field is required'
    }

    if (!repeatPassword) {
      errors.repeatPassword = 'This field is required'
    }

    return errors;
  }

function FormikForm() {
    const register = (values, helpers) => { // (values, { setSubmitting }) => setSubmitting(false)
    console.log(values, helpers);
    // Мы отправили запрос на сервер, а после того как нам пришел ответ -
    // мы самостоятельно выключаем submiting с помощью второго параметра helpers
    helpers.setSubmitting(false)
    }
  
    return (
        <Formik
            initialValues={{
                login: 'admin@test.com',
                password: '',
                repeatPassword: ''
            }}
            onSubmit={register}
            validate={validateForm}
        >
            { (formikProps) => {
                console.log(formikProps );
                return (
                <Form noValidate>
                    <h3>FormikForm</h3>
                    <div>
                        <Field component='input' name='login' type='email' placeholder='Email' />
                        {formikProps.errors.login && formikProps.touched.login && <span className='error'>{formikProps.errors.login}</span>}
                    </div>
                    <div>
                         <Field component='input' name='password' type='password' placeholder='Password' />
                        {formikProps.errors.password && formikProps.touched.password && <span className='error'>{formikProps.errors.password}</span>}
                    </div>
                    <div>
                        <Field component='input' name='repeatPassword' type='password' placeholder='Repeat Password' />
                        {formikProps.errors.repeatPassword && formikProps.touched.repeatPassword && <span className='error'>{formikProps.errors.repeatPassword}</span>}
                    </div>
                    <div>
                        <button type='submit' disabled={formikProps.isSubmitting}>Register</button>
                    </div>
              </Form>
                )
             }}
        </Formik>
    )
  }

export default FormikForm
