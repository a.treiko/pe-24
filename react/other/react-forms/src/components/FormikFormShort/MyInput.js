import React from 'react'

export default function MyInput(props) {
    // console.log(props);  
    const { field, form, ...rest } = props;
    return (
        <div>
            <input {...field} {...rest} />
                {form.errors[field.name] && form.touched[field.name] && <span className='error'>{form.errors[field.name]}</span>}
            </div>
    )
}
