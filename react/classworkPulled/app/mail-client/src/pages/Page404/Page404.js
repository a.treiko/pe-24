import React from 'react';
import './Page404.scss'

function Page404() {
  return (
    <div className='page-404'>
      <h2>404</h2>
      <h3>Page not found</h3>
    </div>
  )
}

export default Page404
