import React from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'
import Inbox from '../pages/Inbox/Inbox'
import Favorites from '../pages/Favorites/Favorites'
import Sent from '../pages/Sent/Sent'
import Page404 from '../pages/Page404/Page404'
import OneEmail from '../pages/OneEmail/OneEmail'

function AppRoutes({emails}) {
  return (
    <Switch>
      <Redirect exact from='/' to='/inbox' />
      <Route exact path='/inbox' render={() => <Inbox emails={emails} />} />
      {/* <Route path='/inbox'><Inbox emails={emails} /></Route> */}
      <Route exact path='/favorites' component={Favorites} />
      <Route exact path='/sent'><Sent /></Route>
      <Route exact path='/emails/:emailId'><OneEmail /></Route>
      <Route path='*'><Page404 /></Route>
    </Switch>
  )
}

export default AppRoutes
