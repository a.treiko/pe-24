import React, { useMemo } from 'react';
import useTimeout from '../../hooks/useTimeout';
import './Footer.scss'

function Footer(){ 
    const numbers = useMemo(() => [1, 2, 3, 4, 5, 6, 7, 8], [])
    // const timeoutNumbers = useTimeout(numbers, 2000)
    return (
        <div>
            <h2>Footer</h2>
            {/* {timeoutNumbers.map(n => <div>{n}</div>)} */}
        </div>
    )
}

export default Footer;
