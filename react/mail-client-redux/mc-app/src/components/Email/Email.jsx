import React, { useState } from 'react'
import './Email.scss'
import Icon from '../Icon/Icon';
import { Link, useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { toggleFavoritesAction } from '../../store/emails/actions';

const Email = (props) => {
    const { email, showFull } = props;
    const history = useHistory();
    const dispatch = useDispatch()


    const goToPrevious = () => {
        history.push(`/emails/${email.id - 1}`)
    }

    const goToNext = () => {
        history.push(`/emails/${email.id + 1}`)
    }

    const toggleFavorites = () => {
        dispatch(toggleFavoritesAction(email))
    }
        return (
            <div>
                <div className='email'>
                    <Link to={`/emails/${email.id}`}>{email.topic}</Link>
                    <Icon type='star' color='gold' filled={email.favorite} onClick={toggleFavorites} />
                </div>
                { showFull && <div className='email__body'>{email.body}</div> }
                {
                     showFull && (
                        <div className='email__controls'>
                            <button onClick={goToPrevious}>Previous</button>
                            <button onClick={goToNext}>Next</button> 
                        </div>
                    )
                }
            </div>
        )
}

export default Email;