import React from 'react';
import './Header.scss';
import PropTypes from 'prop-types';
import { connect } from 'react-redux'
import { getUserSelector } from '../../store/user/selectors';

function Header (props) {
    const { user } = props;

    return (
        <header className='header'>
            <div className='container header__inner'>
                <div>LOGO</div>
                <div className='header__user-info'>
                    {!!user && <div>{user.login}</div>}
                </div>
                <div>MENU</div>
            </div>
        </header>
    )
}

Header.propTypes = {
    title: PropTypes.string,
    user: PropTypes.object
}
Header.defaultProps = {
    title: 'OFFLINE'
}

const mapStateToProps = (state) => {
    return {
        user: getUserSelector(state)
    }
}

export default connect(mapStateToProps)(Header);


