import React from 'react';
import { useSelector } from 'react-redux';
import Email from '../../components/Email/Email';
import { emailsLoadingSelector, getEmailsSelector } from '../../store/emails/selectors';
import './Favorites.scss';

export default function Favorites() {
    const emails = useSelector(getEmailsSelector)
    const isLoading = useSelector(emailsLoadingSelector)

    if (isLoading) {
        return 'Loading...'
    }
    
    const emailCards = emails
        .filter(email => email.favorite)
        .map(email => <Email key={email.id} email={email}/>)

    return (
        <div className='inbox'>
            <div className='container'>
                <div>{emailCards}</div>
            </div>
        </div>
    )
}



