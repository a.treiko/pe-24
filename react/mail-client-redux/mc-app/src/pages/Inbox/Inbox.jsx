import React, { useEffect, useState } from 'react'
import './Inbox.scss'; 
import Email from '../../components/Email/Email'
import { useDispatch, useSelector } from 'react-redux';
import { emailsLoadingSelector, getEmailsSelector } from '../../store/emails/selectors';
import { getEmailsOperation } from '../../store/emails/operations';
import useTimeout from '../../hooks/useTimeout';

function Inbox(){

    const dispatch = useDispatch()
    const emails = useSelector(getEmailsSelector)
    const isLoading = useSelector(emailsLoadingSelector)

    const timeoutEmails = useTimeout(emails)

    useEffect(() => {
        if(emails.length === 0) {
            dispatch(getEmailsOperation())
        }
    },[dispatch])

    if (isLoading) {
        return 'Loading...'
    }
    
    const emailCards = timeoutEmails.map(email => <Email key={email.id} email={email}/>)

    return (
        <div className='inbox'>
            <div className='container'>
                <div>{emailCards}</div>
            </div>
        </div>
    )
}

export default Inbox;
