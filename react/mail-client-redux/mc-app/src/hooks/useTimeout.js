import { useEffect, useState } from 'react'

const useTimeout = (arr, timeout = 1000) => {
    const [timeoutArr, setTimeoutArr] = useState([])

    useEffect(() => {
        setTimeoutArr([])
        arr.forEach((item, index) => {
            setTimeout(() => {
                setTimeoutArr(currentState => {
                    if (currentState.some(el => el.id ===item.id)) {
                        return currentState
                    } else {
                        return [...currentState, item]
                    }
                })
            }, timeout * index + 100)
        })
    },[arr, timeout])

    return timeoutArr;
}

export default useTimeout;