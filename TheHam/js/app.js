
//  SERVICES  ===========================================
// TABS: 
const tabs = document.querySelector('.services-tabs');
// TITLES:
const titles = document.querySelectorAll('.services-tab-item');
// CONTENTS:
const contents = document.querySelectorAll('.services-tabs-content li');
// CLASS ACTIVE:
const activeClass = 'active';

// Массив наших TABS-titles:
const titlesArray = Array.from(document.querySelectorAll('.services-tab-item'));
// Массив наших CONTENT li:
const contentsArray = Array.from(document.querySelectorAll('.services-tabs-content li'));

showCurrentTab();
tabs.addEventListener('click', changeClass);

// Функция смены класса:
function changeClass(event) {

    const tab = event.target.closest('li');

    for (let elem of titles) {
        elem.classList.remove(activeClass);
    }

    tab.classList.add(activeClass);
    showCurrentTab();
}

// Функция демонстрации элемента и присваивание индекса активного 
// элемента к элементу контента по своответственному индексу:
function showCurrentTab() {

    for (let tab of titles) {
        if (tab.classList.contains('active')) {
            for (let elem of contents) {
                elem.style.display = 'none';
            }
            const index = titlesArray.findIndex(elem => elem.classList.contains('active'));
            contentsArray[index].style.display = 'block';
        }
    }
}



//  WORK  ===========================================
const categoryTitle = document.querySelectorAll('.work-tab-item');


categoryTitle.forEach(item => {
    item.addEventListener('click', () => {
        const flipItem = document.querySelectorAll('.work-flip-item');
        categoryTitle.forEach(item => {
            item.classList.remove('work-active')
        });
        item.classList.add('work-active');
        // Показываем фото:
        let titles = item.getAttribute('data-title');
        // console.log(titles);
        flipItem.forEach(show => {
            show.style.display = 'none';
            if (show.getAttribute('data-cat') === titles || titles === 'all') {
                show.style.display = 'block';
            }
        });
    });
});


const graphicDesign = 'graphic-design';
const webDesign = 'web-design';
const landingPages = 'landing-pages';
const wordpress = 'wordpress';
const workGallery = document.querySelector('.work-gallery');

const galleryItemComponent = {
    'graphic-design': `<div class="work-flip-back">
    <div class="flip-back-content">
        <div class="back-content-circles">
            <div class="circle"><i class="fas fa-link"></i></div>
            <div class="circle"><i class="fas fa-square"></i></div>
        </div>
        <div class="back-content-text">
            <p class="back-green-text">Creative Design</p>
            <p class="back-text">Graphic Design</p>
        </div>
    </div>
</div>
</li>`,
    'web-design': `<div class="work-flip-back">
    <div class="flip-back-content">
        <div class="back-content-circles">
            <div class="circle"><i class="fas fa-link"></i></div>
            <div class="circle"><i class="fas fa-square"></i></div>
        </div>
        <div class="back-content-text">
            <p class="back-green-text">Creative Design</p>
            <p class="back-text">Web Design</p>
        </div>
    </div>
</div>
</li>`,
    'landing-pages': `<div class="work-flip-back">
    <div class="flip-back-content">
        <div class="back-content-circles">
            <div class="circle"><i class="fas fa-link"></i></div>
            <div class="circle"><i class="fas fa-square"></i></div>
        </div>
        <div class="back-content-text">
            <p class="back-green-text">Creative Design</p>
            <p class="back-text">Landing Pages</p>
        </div>
    </div>
</div>
</li>`,
    'wordpress': `<div class="work-flip-back">
    <div class="flip-back-content">
        <div class="back-content-circles">
            <div class="circle"><i class="fas fa-link"></i></div>
            <div class="circle"><i class="fas fa-square"></i></div>
        </div>
        <div class="back-content-text">
            <p class="back-green-text">Creative Design</p>
            <p class="back-text">Wordpress</p>
        </div>
    </div>
</div>
</li>`,
};

const buttonLoad = document.querySelector('.load-button');
let counter = 1;
buttonLoad.addEventListener('click', () => {
    // loadingButtonAnimation();
    if (counter === 1) {

        for (let i = 4; i <= 6; i++) {

            workGallery.insertAdjacentHTML('beforeend',
                `<li data-cat="graphic-design" class="work-flip-item all">
            <div class="work-flip-front">
                <img src="./img/${graphicDesign}/img${i}.jpg" alt="work-image">
            </div>
            ${galleryItemComponent["graphic-design"]}`);

            workGallery.insertAdjacentHTML('beforeend',
                `<li data-cat="web-design" class="work-flip-item all">
            <div class="work-flip-front">
                <img src="./img/${webDesign}/img${i}.jpg" alt="work-image">
            </div>
            ${galleryItemComponent["web-design"]}`);

            workGallery.insertAdjacentHTML('beforeend',
                `<li data-cat="landing-pages" class="work-flip-item all">
            <div class="work-flip-front">
                <img src="./img/${landingPages}/img${i}.jpg" alt="work-image">
            </div>
            ${galleryItemComponent["landing-pages"]}`);

            workGallery.insertAdjacentHTML('beforeend',
                `<li data-cat="wordpress" class="work-flip-item all">
            <div class="work-flip-front">
                <img src="./img/${wordpress}/img${i}.jpg" alt="work-image">
            </div>
            ${galleryItemComponent["wordpress"]}`);
        }
        counter++;

    } else if (counter === 2) {
        for (let i = 7; i <= 9; i++) {

            workGallery.insertAdjacentHTML('beforeend',
                `<li data-cat="graphic-design" class="work-flip-item all">
            <div class="work-flip-front">
                <img src="./img/${graphicDesign}/img${i}.jpg" alt="work-image">
            </div>
            ${galleryItemComponent["graphic-design"]}`);

            workGallery.insertAdjacentHTML('beforeend',
                `<li data-cat="web-design" class="work-flip-item all">
            <div class="work-flip-front">
                <img src="./img/${webDesign}/img${i}.jpg" alt="work-image">
            </div>
            ${galleryItemComponent["web-design"]}`);

            workGallery.insertAdjacentHTML('beforeend',
                `<li data-cat="landing-pages" class="work-flip-item all">
                <div class="work-flip-front">
                    <img src="./img/${landingPages}/img${i}.jpg" alt="work-image">
                </div>
                ${galleryItemComponent["landing-pages"]}`);

            workGallery.insertAdjacentHTML('beforeend',
                `<li data-cat="wordpress" class="work-flip-item all">
            <div class="work-flip-front">
                <img src="./img/${wordpress}/img${i}.jpg" alt="work-image">
            </div>
            ${galleryItemComponent["wordpress"]}`);

        }
        counter++;
        buttonLoad.remove()
    }
    
});

function loadingButtonAnimation() {
    buttonLoad.classList.add('loading-animation');
    setTimeout(() => buttonLoad.classList.remove('loading-animation'), 1000);
}

// buttonLoad.addEventListener('click', () => {
//      if (counter === 2) {
//             for (let i = 7; i <= 9; i++) {

//                 workGallery.insertAdjacentHTML('beforeend',
//                     `<li data-cat="graphic-design" class="work-flip-item all">
//                 <div class="work-flip-front">
//                     <img src="./img/${graphicDesign}/img${i}.jpg" alt="work-image">
//                 </div>
//                 ${galleryItemComponent["graphic-design"]}`);

//                 workGallery.insertAdjacentHTML('beforeend',
//                     `<li data-cat="web-design" class="work-flip-item all">
//                 <div class="work-flip-front">
//                     <img src="./img/${webDesign}/img${i}.jpg" alt="work-image">
//                 </div>
//                 ${galleryItemComponent["web-design"]}`);

//                 workGallery.insertAdjacentHTML('beforeend',
//                     `<li data-cat="landing-pages" class="work-flip-item all">
//                     <div class="work-flip-front">
//                         <img src="./img/${landingPages}/img${i}.jpg" alt="work-image">
//                     </div>
//                     ${galleryItemComponent["landing-pages"]}`);

//                 workGallery.insertAdjacentHTML('beforeend',
//                     `<li data-cat="wordpress" class="work-flip-item all">
//                 <div class="work-flip-front">
//                     <img src="./img/${wordpress}/img${i}.jpg" alt="work-image">
//                 </div>
//                 ${galleryItemComponent["wordpress"]}`);

//             }
//             counter++;
//             console.log(counter);
//         }
//     });

    // if (counter === 3) {
    //     buttonLoad.addEventListener('click', () => {
    //         for (i = 7; i <= 9; i++) {
    //             workGallery.insertAdjacentHTML('beforeend', 
    //             `<li data-cat="graphic-design" class="work-flip-item all">
    //             <div class="work-flip-front">
    //                 <img src="./img/${graphicDesign}/img${i}.jpg" alt="work-image">
    //             </div>
    //             ${galleryItemComponent["graphic-design"]}`);

    //             workGallery.insertAdjacentHTML('beforeend', 
    //             `<li data-cat="web-design" class="work-flip-item all">
    //             <div class="work-flip-front">
    //                 <img src="./img/${webDesign}/img${i}.jpg" alt="work-image">
    //             </div>
    //             ${galleryItemComponent["web-design"]}`);

    //             workGallery.insertAdjacentHTML('beforeend', 
    //                 `<li data-cat="landing-pages" class="work-flip-item all">
    //                 <div class="work-flip-front">
    //                     <img src="./img/${landingPages}/img${i}.jpg" alt="work-image">
    //                 </div>
    //                 ${galleryItemComponent["landing-pages"]}`);

    //             workGallery.insertAdjacentHTML('beforeend', 
    //             `<li data-cat="wordpress" class="work-flip-item all">
    //             <div class="work-flip-front">
    //                 <img src="./img/${wordpress}/img${i}.jpg" alt="work-image">
    //             </div>
    //             ${galleryItemComponent["wordpress"]}`);
    //         }
    //     });
    //     buttonLoad.remove();
    // }