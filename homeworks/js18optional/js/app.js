
// Реализовать функцию полного клонирования объекта. 
// Технические требования:

// Написать функцию для рекурсивного полного клонирования объекта 
// (без единой передачи по ссылке, внутренняя вложенность свойств 
// объекта может быть достаточно большой).
// Функция должна успешно копировать свойства в виде объектов и 
// массивов на любом уровне вложенности.
// В коде нельзя использовать встроенные механизмы клонирования, 
// такие как функция Object.assign() или спред-оператор.


let obj = {
    'first key': 1,
    'second key': 2,
    'third key': {
        'subfirst key': 5,
        'subsecond key': 6,
    },
};

console.log(obj);

function deepCopy(obj) {
    let copiedObject;

    // Если объект - массив, тогда массив, иначе объект
    if (Array.isArray(obj)) {
        copiedObject = [];
    } else {
        copiedObject = {};
    }

    // Пробигаемся по ключам объекта
    // если ключ является объектом с свойствами - вызываем для него рекурсию:
    for (let key in obj) {
        if (typeof obj[key] === 'object') {
           copiedObject[key] = deepCopy(obj[key]);
        } else {
            copiedObject[key] = obj[key];
        }
    }
    return copiedObject;
}

let cloneObj = deepCopy(obj);
console.log(cloneObj);
console.log(cloneObj === obj);
console.log(cloneObj == obj);

cloneObj['first key'] = 'changed';
console.log(cloneObj);
console.log(obj);


