
const buttons = document.querySelectorAll('.btn');

document.addEventListener('keydown', highlightLetters);

function highlightLetters(event) {
   buttons.forEach(btn => btn.classList.contains('active') ? btn.classList.remove('active') : false);
   buttons.forEach(btn => event.key.toUpperCase() === btn.textContent.toUpperCase() ? btn.classList.add('active') : false);
   console.log(event);
}

// В современном мире есть альтернативные способы ввода в поле input:
// голосовым сообщением или с помощью Copy/Paste
// Потому, можно сделать вывод, что событий клавиатуры - недостаточно.
