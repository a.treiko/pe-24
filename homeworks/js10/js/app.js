// Написать реализацию кнопки "Показать пароль".

// Технические требования:

// В файле index.html лежит разметка для двух полей ввода пароля.
// По нажатию на иконку рядом с конкретным полем - должны отображаться символы, 
// которые ввел пользователь, иконка меняет свой внешний вид.
// Когда пароля не видно - иконка поля должна выглядеть, как та, 
// что в первом поле (Ввести пароль)
// Когда нажата иконка, она должна выглядеть, как та, что во втором поле 
// (Ввести пароль)
// По нажатию на кнопку Подтвердить, нужно сравнить введенные значения в полях
// Если значения совпадают - вывести модальное окно (можно alert) с текстом - You are welcome;
// Если значение не совпадают - вывести под вторым полем текст красного цвета  Нужно ввести одинаковые значения

// После нажатия на кнопку страница не должна перезагружаться
// Можно менять разметку, добавлять атрибуты, теги, id, классы и так далее

// Данные:
const passwordForm = document.querySelector('.password-form');
const itemsPassword = document.querySelectorAll('.icon');
const btnConfirm = document.querySelector('.btn');
const enterPassword = document.querySelector('#password');
const checkPassword = document.querySelector('#password-check');

// Error-message:
const errorMessage = document.createElement('span');
errorMessage.innerText = 'You must enter the same values!';
errorMessage.style.cssText = `position: absolute;
                            top: 110px;
                            left: 300px`;
errorMessage.classList.add('validation');
errorMessage.classList.add('hidden');
btnConfirm.before(errorMessage);

// События элементов глаза и кнопки:
itemsPassword.forEach((item) => item.addEventListener('click', showPassword));
btnConfirm.addEventListener('click', confirmPassword);

// Функция отображения пароля:
function showPassword(event) {
    console.log(event.target);
    // <i class="fas fa-eye-slash"></i>
    // Проверяем на наличие данного ID в инпуте:
    if (event.target.id === 'password-1') {
        // Если тип нашего инпута - пароль, тогда добаляем перечеркнутый глаз и задаем инпуту артрибут тип - текст:
        if (enterPassword.getAttribute('type') === 'password') {
            event.target.classList.add('fa-eye-slash');
            enterPassword.setAttribute('type', 'text');
        } else {
            event.target.classList.remove('fa-eye-slash');
            enterPassword.setAttribute('type', 'password');
        }
    } else {
        if (checkPassword.getAttribute('type') === 'password') {
            event.target.classList.add('fa-eye-slash');
            checkPassword.setAttribute('type', 'text');
        } else {
            event.target.classList.remove('fa-eye-slash');
            checkPassword.setAttribute('type', 'password');
        }
    }
}

// Функция подтверждения пароля:
function confirmPassword(event) {
    event.preventDefault() 
    if (enterPassword.value && checkPassword.value) {
        if (enterPassword.value === checkPassword.value) {
            alert('Welcome!')
        } else {
            errorMessage.classList.remove('hidden');
        }
    } else {
        alert('Enter your password!')
    }
}





