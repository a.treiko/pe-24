// Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)
// Задание
// Реализовать функцию, которая будет получать массив элементов 
// и выводить их на страницу в виде списка. 

// Технические требования:
// Создать функцию, которая будет принимать на вход массив.
// Каждый из элементов массива вывести на страницу в виде пункта списка
// Необходимо использовать шаблонные строки и функцию map массива для формирования 
// контента списка перед выведением его на страницу.

// Примеры массивов, которые можно выводить на экран:
['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']
['1', '2', '3', 'sea', 'user', 23]
// Можно взять любой другой массив.

// Необязательное задание продвинутой сложности:
// ??? Очистить страницу через 10 секунд. Показывать таймер обратного 
// отсчета (только секунды) перед очищением страницы.
// Если внутри массива одним из элементов будет еще один массив или 
// объект, выводить его как вложенный список.
let myArray = ['habr', 'youtube', 'gitlab', {name: 'Artem', age: 28, learn: 'JavaScript'}, ['Saint Vie', 'Satori', 'Oliver Schories']];


let showList = (array) => {
    let ol = document.createElement('ol');
    document.body.append(ol);
    // Перебираем каждую li в ol
    // если элемент будет массивом - создаем для него подсписок  li:
    let list = array.map((item) => {
        if (Array.isArray(item)) {
            let subList = item.map((subItem) => {
                return `<li>${subItem}</li>`;
            });
            // Возвращаем список li внутри которого маркерованный список и пререводим в строку наш массив
            return `<li><ol>${subList.join('')}</ol></li>`;
        }
        // Если li - объект
        else if (typeof item === 'object') {
            let objList = '';
            // Переберем его ключи и дабавим в нашу переменную objList в виде <li> ключ : значение ключа </li>
            for (let objItem in item) {
                objList += `<li>${objItem}: ${item[objItem]}</li>`;
            }
            // Возвращаем список li внутри которого нумерованный список
            return `<li><ul>${objList}</ul></li>`;
        }
        return `<li>${item}</li>`
    });
    ol.innerHTML = list.join('');
}

// showList(myArray);
// ' append() - добав. в конец node
// ' join() объеденяет элементы массива в строку
// ' clearInterval() - отменяет многократные повторения дейсвия, установленные вызовом функции

window.addEventListener('load', () => {
    showList(myArray);
    showTimer(15);
});

// Таймер
let showTimer = (sec) => {
    // Общий контейнер для таймера
    let container = document.createElement('div');
    document.body.append(container);
    container.innerHTML = 'Time Left: ';
    // Таймер
    let timer = document.createElement('span');
    // Закидываем таймер в контейнер
    container.append(timer);
    // Отображаем в таймере отсчет:
    timer.innerHTML = sec;
    
    let timeLeft = +(timer.innerHTML);
    let timeFunc = setInterval(() => {
        // Пока timeLeft больше 0
        if (timeLeft >= 0) {
            // Минисуем отсчет отображенный в таймере
            timer.innerHTML = timeLeft--;
        } else {
            // Иначе - очистить содержание и очистить интервал. 
            document.body.innerHTML = '';
            clearInterval(timeFunc);
        }
    }, 1000)
}



// DOM - набор связанных объектов, созданных браузером при парсинге.
