// Опишите своими словами разницу между функциями setTimeout() и setInterval().
// Что произойдет, если в функцию setTimeout() передать нулевую задержку? Сработает ли она мгновенно, и почему?
// Почему важно не забывать вызывать функцию clearInterval(), когда ранее созданный цикл запуска вам уже не нужен?

// В папке banners лежит HTML код и папка с картинками.
// При запуске программы на экране должна отображаться первая картинка.
// Через 10 секунд вместо нее должна быть показана вторая картинка.
// Еще через 10 секунд - третья.
// Еще через 10 секунд - четвертая.
// После того, как покажутся все картинки - этот цикл должен начаться заново.
// При запуске программы где-то на экране должна появиться кнопка с надписью Прекратить.
// По нажатию на кнопку Прекратить цикл завершается, на экране остается показанной та картинка, 
// которая была там при нажатии кнопки.
// Рядом с кнопкой Прекратить должна быть кнопка Возобновить показ, при нажатии которой цикл 
// продолжается с той картинки, которая в данный момент показана на экране.
// Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.

document.addEventListener('DOMContentLoaded', () => {loopImages()});

const loopImages = () => {
    let images = document.querySelectorAll('img');
    let stopBtn = document.createElement('button');
    let continueBtn = document.createElement('button');

    stopBtn.textContent = 'Stop';
    continueBtn.textContent = 'Continue';

    document.body.prepend(stopBtn);
    stopBtn.after(continueBtn);

    let currentIndex = 0;

    images.forEach(item => {
        item.style.cssText = `
                            position: absolute;
                            display: none;
                            top: 0;
                            left: 0;`;
    });
    images[0].style.display = 'block';

    let working = false;

    const changeImage = () => {
        working = true;
        images[currentIndex].style.display = 'none';

        if (currentIndex === images.length - 1) {
            currentIndex = 0;
        } else {
            currentIndex++;
        }
        images[currentIndex].style.display = 'block';
    };

    let startChangeImages = setInterval(changeImage, 10000);

    continueBtn.addEventListener('click', () => {
        if (!working) startChangeImages = setInterval(changeImage, 2000);
    });

    stopBtn.addEventListener('click', () => {
        clearInterval(startChangeImages);
        working = false;
    })
}


// setTimeOut() - выполняет действие с отсрочкой времени единожды.
// setInterval() - выполняет код циклично, через указанный промежуток времни, 
// где промежуток мы указываем втромым аргументом.
// setTimeOut() - с переданной нулевой задержкой не выполнится моментально, 
// если будет иметь код дейсвия в теле. Если в теле не будет кода - выполнится моментально.
// clearInterval() - очищает заданный интервал в setInterval().
// Ее вызывают, чтоб не перегружать браузер очередными обработками функции.