// Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования

// Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем.

// Технические требования:

// Возьмите выполненное домашнее задание номер 4 
// (созданная вами функция createNewUser()) и дополните ее следующим 
// функционалом:

// При вызове функция должна спросить у вызывающего дату рождения 
// (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
// Создать метод getAge() который будет возвращать сколько пользователю лет.
// Создать метод getPassword(), который будет возвращать первую букву 
// имени пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) 
// и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).

// Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.
function createUser() {
    const newUser = {};

    newUser.firstName = prompt('Enter your name');
    // Второй способ обращения к свойствам объекта
    newUser['lastName'] = prompt('Enter your surname');
    let userDate = prompt('Enter your birthday', 'dd.mm.yyyy');
    // Разбили на массив строк отделяя точками, перевернули значение и объеделини массив строк в строку
    newUser.birthday = new Date(userDate.split('.').reverse().join(', '));
    
    // Дескриптор
    Object.defineProperties(newUser, {
        firstName: {writable: false},
        lastName: {writable: false},
    });
    newUser.setFirstName = function(newFirstName) {
        Object.defineProperty(this, 'firstName', {writable: true});
        this.firstName = newFirstName;
        Object.defineProperty(this, 'firstName', {writable: false});
    },
    newUser.setLastName = function(newLastName) {
        Object.defineProperty(this, 'lastName', {writable: true});
        this.lastName = newLastName;
        Object.defineProperty(this, 'lastName', {writable: false});
    },
    newUser.getAge = function() {
        let currentDate = new Date();
        let age;
        if (this.birthday.getMonth() <= currentDate.getMonth()) {
            currentDate.getFullYear() - this.birthday.getFullYear();
        } else {
            currentDate.getFullYear() - this.birthday.getFullYear() - 1;
        }
        return age;

    },
    newUser.getPassword = function() {
        const password = this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.getFullYear();
        return password
    },
    newUser.getLogin = function() {
        const login = (this.firstName[0] + this.lastName).toLowerCase();
        return login;
    }

    return newUser;

}

const firstUser = createUser();
console.log(firstUser.getAge());
console.log(firstUser.getLogin());
console.log(firstUser.getPassword());


// Экранирование используется для того, чтоб стоящий за ним символ не интерпритировался
// как код. В строках где есть апострофы - мы их экранируем. Или используем разные скобы