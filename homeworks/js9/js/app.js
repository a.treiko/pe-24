
// TABS:
let tabs = document.querySelector('.tabs');
// TITLE:
let titles = document.querySelectorAll('.tabs-title');
// CONTENT LI: 
let contents = document.querySelectorAll('.tabs-content li');
// ACTIVE:
let activeClass = 'active';

// Массив наших tabs-title:
const titlesArray = Array.from(document.querySelectorAll('.tabs-title'));
// Массив наших CONTENT li:
const contentsArray = Array.from(document.querySelectorAll('.tabs-content li'));

// Вызывам нашу функицю
showCurrentTab();
tabs.addEventListener('click', changeClass);

// Функия смены класса:
function changeClass(event) {
    // Присваеваем в tab ссылку на объект li:
    const tab = event.target.closest('li');

    // Перебираем наш псевдомассив и убираем класс active:
    for (let elem of titles) {
        elem.classList.remove(activeClass);
    }
    // И добавляем класс active на объект, который нажал пользователь:
    tab.classList.add(activeClass);
    showCurrentTab();
}

// Функция, которая находит элемент с классом active у tabs и возвращает его
// Перебираем элементы контента и прячим их
// Далее, возвращаем индекс нашего активного tab.
// Для соответственного элемента по индексу в contents - присваиваем display: block;
function showCurrentTab() {
    // Перебираем наш псевдомассив  titles
    for (let tab of titles) {
        // Если у элемента есть класс active:
        if (tab.classList.contains('active')) {
            // Перебираем наш псевдомассив contents и прячем элементы:
            for (let elem of contents) {
                elem.style.display = 'none';
            }
            // Создаем переменную для обнаружения индекса активного элемента в tabs(используем массив):
            const index = titlesArray.findIndex(elem => elem.classList.contains('active'));
            // Для аналогичного элемента по индексу в контенте - присваиваем display: block (используем массив):
            contentsArray[index].style.display = 'block';
        }
    }
}



// closest - возвращает ближайший родительский элемент (или сам элемент), 
// который соответствует заданному CSS-селектору или null

// classList.contain - проверяет на наличие класса у элемента
// Вернет: TRUE || FALSE


