// Опишите своими словами, что такое метод обьекта
// Задание
// Реализовать функцию для создания объекта "пользователь". 

// Технические требования:
// Написать функцию createNewUser(), 
// которая будет создавать и возвращать объект newUser.
// При вызове функция должна спросить у вызывающего имя и фамилию.
// Используя данные, введенные пользователем, создать объект newUser 
// со свойствами firstName и lastName.
// Добавить в объект newUser метод getLogin(), который будет возвращать 
// первую букву имени пользователя, соединенную с фамилией пользователя, 
// все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
// Создать пользователя с помощью функции createNewUser(). 
// Вызвать у пользователя функцию getLogin(). Вывести в консоль результат 
// выполнения функции.


// Необязательное задание продвинутой сложности:

// Сделать так, чтобы свойства firstName и lastName нельзя было изменять напрямую. 
// Создать функции-сеттеры setFirstName() и setLastName(), которые позволят изменить 
// данные свойства.

function createUser() {
    const newUser = {};

    newUser.firstName = prompt('Enter your name');
    // Второй способ обращения к свойствам объекта
    newUser['lastName'] = prompt('Enter your surname');
    
    // Дескриптор
    Object.defineProperties(newUser, {
        firstName: {writable: false},
        lastName: {writable: false},
    });
    newUser.setFirstName = function(newFirstName) {
        Object.defineProperty(this, 'firstName', {writable: true});
        this.firstName = newFirstName;
        Object.defineProperty(this, 'firstName', {writable: false});
    }
    newUser.setLastName = function(newLastName) {
        Object.defineProperty(this, 'lastName', {writable: true});
        this.lastName = newLastName;
        Object.defineProperty(this, 'lastName', {writable: false});
    }

    return newUser;

}

// Функция, которая будет генерировать пароль:
function getLogin() {
    const login = (this.firstName[0] + this.lastName).toLowerCase()
    return login;
}

// Создадим firstUser
const firstUser = createUser();
// Присвоим firstUser функцию getLogin
firstUser.getLogin = getLogin;
console.log(firstUser.getLogin());
// Зададим fistName и lastName для firstUser
firstUser.setFirstName('Lord');
firstUser.setLastName('Voldemort');
console.log(firstUser.getLogin());
console.log(firstUser);

// Метод объекта - это функция у объекта, которая является свойством объекта.


