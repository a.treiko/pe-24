// Создать объект студент "студент" и проанализировать его табель.

// Технические требования:

// Создать пустой объект student, с полями name и last name.
// Спросить у пользователя имя и фамилию студента, полученные 
// значения записать в соответствующие поля объекта.
// В цикле спрашивать у пользователя название предмета и оценку по нему. 
// Если пользователь нажмет Cancel при n-вопросе о названии предмета, закончить цикл. 
// Записать оценки по всем предметам в свойство студента tabel.
// Посчитать количество плохих (меньше 4) оценок по предметам. 
// Если таких нет, вывести сообщение Студент переведен на следующий курс.
// Посчитать средний балл по предметам. Если он больше 7 - 
// вывести сообщение Студенту назначена стипендия.


const student = {
    name: null,
    'last name': null,
};

let table = {};
student.table = table;

student.name = prompt('Enter student name');
student['last name'] = prompt('Enter student last name');

let key; 

while (key !== null) {
    key = prompt('Enter the name of subject');
    if (key !== null) {
        do {
            table[key] = +prompt(`Enter your mark of ${key}`);
        } while (!isFinite(table[key]))
    }
}

console.log(student);

let badMarksCount = 0;
let sumMarks = 0;
let markCount = 0;

for (let key in student.table) {
    if (student.table[key] < 4) {
        ++badMarksCount;
    }
    sumMarks += student.table[key];
    ++markCount;
}

if (badMarksCount == 0) {
    console.log(`Student transferred to the next course`);
}
let averageMark = Math.round(sumMarks/markCount);
if (averageMark > 7) {
    console.log(`Student assigned a scholarship`);
}
console.log(badMarksCount);
console.log(sumMarks);
console.log(markCount);
console.log(student);

