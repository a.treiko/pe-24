// Опишите своими словами, как Вы понимаете, что такое обработчик событий.

// Создать поле для ввода цены с валидацией.

// При загрузке страницы показать пользователю поле ввода (input) с надписью Price. 
// Это поле будет служить для ввода числовых значений
// Поведение поля должно быть следующим:

// При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. 
// При потере фокуса она пропадает.
// Когда убран фокус с поля - его значение считывается, над полем создается span, 
// в котором должен быть выведен текст: Текущая цена: ${значение из поля ввода}. 
// Рядом с ним должна быть кнопка с крестиком (X). Значение внутри поля ввода 
// окрашивается в зеленый цвет.
// При нажатии на Х - span с текстом и кнопка X должны быть удалены. 
// Значение, введенное в поле ввода, обнуляется.
// Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода 
// красной рамкой, под полем выводить фразу - Please enter correct price. 
// span со значением при этом не создается.

// В папке img лежат примеры реализации поля ввода и создающегося span.


// ОБЕРТКА:
const wrapper = document.createElement('div');
wrapper.classList.add('wrapper');
wrapper.style.cssText = `
    font-family: 'Arial', sans-serif;
    background-color: salmon;
    padding: 2em;
    width: 22em;

`;
document.body.append(wrapper);

// ФОРМА:
const form = document.createElement('form');
form.classList.add('form');
form.style.cssText = `
    display: inline-block;
    position: reletive;
    border: 1px solid yellow;
    padding: 2em;
`;
wrapper.append(form);

// ИНПУТ:
const input = document.createElement('input');
input.type = 'number';
input.name = 'price';
input.placeholder = '30000';
input.classList.add('input');
input.style.cssText = `
    border: none;
    font-size: 1rem;
    padding: 0 0.5em;
`;
form.append(input);

// СПАН-ТЕКСТ:
const span = document.createElement('span');
span.classList.add('span-text');
span.textContent = 'Price $: ';
form.prepend(span);

// СПАН 'showPrice':
const showPrice = document.createElement('span');
showPrice.classList.add('show-price');
form.prepend(showPrice);

// СПАН 'errorMessage':
const errorMessage = document.createElement('span');
errorMessage.classList.add('error-message');
form.append(errorMessage);

// Событие фокуса
input.addEventListener('focus', addFocus);
function addFocus(event) {
    const elem = event.target.closest('.input');
    elem.classList.remove('focused');
    elem.classList.remove('green-text');
    elem.classList.remove('error');
    elem.classList.add('focused');

    if (elem.value >= 0) {
        document.querySelector('show-price').innerHTML = '';
    } else {
        elem.classList.remove('error');
        document.querySelector('error-message');
    }
}

input.addEventListener('blur', removeFocus);
function removeFocus(event) {
    const elem = event.target.closest('input');
    elem.classList.remove('focused');
    if (elem.value) {
        if (elem.value >= 0) {
            showPrice.classList.add('show-price');
            showPrice.innerHTML = `Текущая цена ${input.value}`;
            form.insertAdjacentElement('beforebegin', showPrice);
            input.showPrice = showPrice;
            showPrice.insertAdjacentElement ('beforebegin', '<button class="clean"> x </button>');
            input.classList.add('green-text');
            document.querySelector('.show-price').onclick = function() {
                showPrice.classList.add('hidden');
                input.value = '';
            }
        } else {
            input.classList.add('error');
            input.insertAdjacentHTML('afterbegin', '<span class="error-message"> Enter a correct price </span>')
        }
    }
}














// blur -  событие вызывается, когда теряется фокус

// event.target - сылка на объект

// closest - возвращает ближайший родительский элемент или сам элмент,
// который соответсвует данному селектору.

// insertAdjacentElement -  добавляет переданный элемент,
// относительно элемента, вызвашего метод

