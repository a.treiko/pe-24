// Опишите своими словами, как Вы понимаете, что такое обработчик событий.

// Создать поле для ввода цены с валидацией.

// При загрузке страницы показать пользователю поле ввода (input) с надписью Price. 
// Это поле будет служить для ввода числовых значений
// Поведение поля должно быть следующим:

// При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. 
// При потере фокуса она пропадает.
// Когда убран фокус с поля - его значение считывается, над полем создается span, 
// в котором должен быть выведен текст: Текущая цена: ${значение из поля ввода}. 
// Рядом с ним должна быть кнопка с крестиком (X). Значение внутри поля ввода 
// окрашивается в зеленый цвет.
// При нажатии на Х - span с текстом и кнопка X должны быть удалены. 
// Значение, введенное в поле ввода, обнуляется.
// Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода 
// красной рамкой, под полем выводить фразу - Please enter correct price. 
// span со значением при этом не создается.

// В папке img лежат примеры реализации поля ввода и создающегося span.


const body = document.querySelector('body');
// ОБЕРТКА
const wrapper = document.createElement('div');
wrapper.classList.add('wrapper');
wrapper.style.cssText = `
    font-family: 'Arial', sans-serif;
    background-color: #F08080;
    width: 20em;
    padding: 2em;
`;
body.append(wrapper);

// ФОРМА
const form = document.createElement('form');
form.classList.add('form');
form.style.cssText = `
    padding: 1em 0.5em;
`;
wrapper.append(form);

// ЛЕЙБЛ
const label = document.createElement('label');
label.textContent = 'PRICE: ';
label.style.cssText = `
    padding 0.5em;
    font-size 1.5rem;
`;
form.append(label);

// ИНПУТ
const input = document.createElement('input');
input.classList.add('input');
input.setAttribute('type', 'number');
input.style.cssText = `
    padding: 0 0.5em;
    font-size: 1rem;
    border: 1px solid grey;
    border-radius: 4px
`;
label.append(input);

// СПАН
const span = document.createElement('span');
span.style.cssText = `

`;
wrapper.append(span);

// BUTTON
const btn = document.createElement('button');
btn.innerHTML = '&times';
btn.style.cssText = `
    broder-radius: 50%;
    cursor: pointer;
    margin: 0.5em 0.5em;
`;

// Событие на фокус:
input.addEventListener('focus', () => {
    input.style.cssText = `
        border: 1px solid darkgreen;
        border-radius: 4px;
        color: darkgreen;
    `;
});

// Событие сброса фокуса
input.addEventListener('blur', () => {
    if (input.value < 0 && input.value !== '') {
        btn.remove();
        form.after(span);
        span.textContent = 'Enter correct price';
        span.style.borderColor = 'darkred';
    } else if (input.value >= 0 && input.value !== '') {
        form.before(span);
        span.after(btn);
        span.textContent = `Текущая цена: ${input.value}`;
        input.style.textContent = `
            border: 1px solid black;
            color: darkgreen;
        `;
        btn.addEventListener('click', () => {
            span.remove();
            btn.remove();
            input.value = '';
        });
    } else {
        span.remove();
        btn.remove();
    }
});
  



// blur -  событие вызывается, когда теряется фокус

// event.target - сылка на объект

// closest - возвращает ближайший родительский элемент или сам элмент,
// который соответсвует данному селектору.

// insertAdjacentElement -  добавляет переданный элемент,
// относительно элемента, вызвашего метод


// Обработик событий - это явление, с помощью которого JavaScript реагирует
// на дейсвия пользователя. 
