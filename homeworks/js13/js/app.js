
// Если пользователь не выбирал тему, то тема light - установленна по стандарту:
if (!localStorage.theme) localStorage = 'light';
// Оставляем выбранную тему ранее с помощью localStotage
// и получаем dark тему даже после обновления:
document.body.className = localStorage.theme;

let toggleBtn = document.getElementById('toggle-btn');
toggleBtn.onclick = () => {
    // Добавляем на body класс dark при клике на кнопку:
    document.body.classList.toggle('dark');
    // Текс кнопки = Проверяем, есть ли класс dark у body: если есть, то текст light, если нету, то текст dark:
    toggleBtn.textContent = document.body.classList.contains('dark') ? 'To light theme' : 'To dark theme';
    localStorage.theme = document.body.className || 'light';
}