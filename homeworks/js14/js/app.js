'use strict'

const $ = window.$;

$(document).ready(function() {

    $('.header-nav').on('click', 'a', function() {
        const topCoordinate = $($(this).attr('href')).offset().
        $('html').animate({scrollTop: topCoordinate}, 1000);
    })
})