// Создать элемент h1 с текстом «Добро пожаловать!».
// При клике по кнопке менять цвет каждой буквы элемента h1 на случайный.

// Дано:
const PHRASE = "Добро пожаловать!";

function getRandomColor() {
	const r = Math.floor(Math.random() * 255);
	const g = Math.floor(Math.random() * 255);
	const b = Math.floor(Math.random() * 255);

	return `rgb(${r}, ${g}, ${b})`;
}
const body = document.querySelector('body');

const title = document.createElement('h1');
title.textContent = PHRASE;
body.append(title);

const btn = document.createElement('button');
btn.textContent = 'Раскрасить';
body.append(btn);

btn.addEventListener('click', () => {
    // Разбиваем нашу фразу на массив строк
    let newArr  = PHRASE.split('').map((item) => {
        let span = document.createElement('span');
        // Для каждой буквы свой SPAN для применения стилей
        span.textContent = item;
        // Применяем функцию, которая возвращает случайный цвет для каждого SPAN
        span.style.color = getRandomColor();
        return span;
    });
    const fragment = document.createDocumentFragment();
    fragment.append(...newArr);

    title.innerHTML = '';
    title.append(fragment);
})

// split - разбивает объеки String на массив строк
// map - создает новый массив с результатом применения функции к каждому элементу массива
