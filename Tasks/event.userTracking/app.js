
// На странице расположено текстовое поле textarea. 
// Изначально на странице ничего не происходит. 
// Когда польователь начинает вводить данные в поле 
// (поле переходит в статус фокуса) начинает работ функция 
// userTracking. Когда пользователь останавливает или 
// прекращает ввод данных (поле теряет фокус больше чем на 2 
// секунды) функция выводит сообщение: "Заполните поле!".     
// Условием "заполненого" поля считается наличие 20 символов 
// в значении поля.


function setTimer() {
    if(text.value.length < 20) {
        timer = setTimeout(showAlert, 2000);
    }
}

function showAlert() {
    alert('Заполните поле!');
    clearTimeout(timer);
}

function userTracking() {
    text.addEventListener('blur', setTimer);
}

const text = document.querySelector('textarea');
let timer;

text.addEventListener('focus', () => {
    userTracking();
});

document.addEventListener('keyup', setTimer);
document.addEventListener('keydown', () => {
    if (text.value.length >= 20) clearTimeout(timer);
})


// function setTimer() {
//     if (text.value.length < 20) {
//         timer = setTimeout(showAlert, 2000);
//     }
// }

// function showAlert() {
//         alert('Заполните поле');
//         clearTimeout(timer);
// }

// function funcDelay() {
//     timer = setTimeout(showAlert, 2000);
// }

// let timer;
// function userTracking() {
//     text.addEventListener('blur', () => {
//     })
// }

// const text = document.querySelector('textarea');
// text.addEventListener('focus', funcDelay);

// document.addEventListener('keyup', funcDelay);
// document.addEventListener('ketdown', function() {
//     if (text.value.length >= 20) clearTimeout(timer);
// })

