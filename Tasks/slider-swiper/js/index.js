
// Инициализация Swiper:
new Swiper('image-slider', {
    // Стрелки:
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },

    // Пагинация, буллеты, прогрессор:
    pagination: {
        el: '.swiper-pagination',
        // Буллеты:
        clickable: true,
        // Динамические буллеты:
        dynamicBullets: true,
        // Фракция:
        // type: 'fraction',
        // Прогрессбар:
        // type: 'progressbar'
    },

    // Скролл:
    scrollbar: {
        el: '.swiper-scrollbar',
        // Возможность перетаскивать ползунок:
        draggable: true,
    },

    // Переключение при клике на слайд:
    slideToClickedSlide: true,
    // Хэш-навигация. В HTML для каждого слайда дописать data-hash:
    hashNavigation: {
        // Отслеживать состояние:
        watchState: true,

    },

    // Управление клавиатурой:
    keyboard: {
        // Включить/выключить:
        enabled: true,
        // Включить/выключить слайдер в пределаю вьюпорта:
        onlyInVuewport: true,
        // Управление кнопками:
        pageUpDown: true,
    }, 

    // Управление колесом мыши:
    mouseweel: {
        // Чувствительность:
        sensitivity: 1,
        // Класс объекта, на котором будет срабатывать прокрутка:
        // eventsTarget: '.image-slider',
    },

    // Автовысота:
    autoHeight: true,

    // Количество слайдов для показа:
    // * можно указывать нецелые числа (2.5 или 2.8)
    slidesPerView: 3,

    // Отступ между слайдами:
    spaceBetween: 30,

    // Кол-во пролистываемых слайдов:
    slidePerGroup: 3,

    // Активный по центру:
    centredSlides: true,

    // Стартовый слайд (отсчет с 0):
    initialSlide: 0,

    // Мультирядность:
    // * Выставить autoHeight: false,
    // * css: для всего слайдера выставить высоту
    //        и для самих слайдов тоже выставить высоту
    slidesPerColumn: 2,

    // Бесконечный слайдер:
    // * не будет работать с мультирядностью
    loop: false,
    // Кол-во дублируемых слайдов:
    loopedSlides: 3,

    // Свободный режим:
    // * хорошо работает с скролом мыши
    freeMode: true,

    

});