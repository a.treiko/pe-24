
const tabsBtn = document.querySelectorAll('.tabs_nav-btn');
const tabsItems = document.querySelectorAll('.tabs_item');

tabsBtn.forEach(onTabClick);

function onTabClick(item) {
    item.addEventListener('click', function() {
        // Получаем data атрибуты, которые соответствуют названиям id
        let tabId = item.getAttribute('data-tab');
        // console.log(tabId); //: #tab_1
        // Текущий слайд будем находить по CSS селектору - #tab_n 
        let currentTab = document.querySelector(tabId);

        // Если класса active нету - выполняем следующие действия:
        if (!item.classList.contains('active')) {
            tabsBtn.forEach(function(item) {
                item.classList.remove('active');
            });
    
            tabsItems.forEach(function(item) {
                item.classList.remove('active');
            });
    
            item.classList.add('active');
            currentTab.classList.add('active');
        }
        
    });
}



