// На странице расположен input. Создать функцию, которая отслеживает вводимые данные и запрещает вводить заглавные буквы. 
// При вводе больших букв выводить сообщение "Заглавные буквы вводить запрещено!".

const input = document.querySelector('input');

document.addEventListener('keydown', (event) => {
    
    if (event.key.length === 1) {
        if (event.key.charCodeAt(0) >= 65 && event.key.charCodeAt(0) <= 90 ||
        event.key.charCodeAt(0) >= 1040 && event.key.charCodeAt(0) <= 1065 || 
        event.key.charCodeAt(0) === 1025) {
           console.log('Заглавные буквы вводить запрещено!')
   }
}
})