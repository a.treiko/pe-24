
// Добавить метод call, чтоб вывелось в консоль value инпута:

document.addEventListener('DOMContentLoaded', function() {

    const btn = document.getElementById('btn');
    btn.onclick = onBtnClick;

})

function onBtnClick() {
    const elem = document.getElementById('elem');
    doSomeMagic.call(elem)
}
// Эту функцию можно вынести в отдельный файл и использовать в коде:
function doSomeMagic() {
    console.log(`output for input --> ${this.value}`);
}