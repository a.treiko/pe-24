
// Есть функция sum, которая суммирует все элементы массива:

function sum(arr) {
    return arr.reduce(function(a, b) {
        return a + b;
    });
}
console.log(sum([1, 2, 3])); //: 6

// Создать аналогичную функцию sumArgs(), 
// которая будет суммировать все свои аргументы.
// Для решерия применить reduce к arguments используя call, apply или одалживание метода

function sumArgs() {
    return Array.prototype.reduce.call(arguments, function(a, b) {
        return a + b
    });
}

console.log(sumArgs(1, 2, 3, 4, 5));



