

// Сделайте функцию, каждый вызов которой 
// будет генерировать случайные числа от 1 до 100. Но так, 
// чтоб они не повторялись, пока не будут перебраны все числа из этого промежутка.


function uniqueMaker() {
    let arr = [];
    return function() {
        if (arr.length === 100) {
            arr = []
        }
        let a;
        do {
            a = Math.floor(Math.random() * 100);
        } while (arr.includes(a));

        arr.push(a);
        console.log(arr);
        return a;
    }
}


const func = uniqueMaker();
console.log(func());
console.log(func());
console.log(func());
console.log(func());
console.log(func());

