// Обьясните своими словами, как вы понимаете, как работает прототипное наследование в Javascript

// Реализовать класс Employee, в котором будут следующие свойства 
// - name (имя), age (возраст), salary (зарплата). Сделайте так, чтобы 
// эти свойства заполнялись при создании объекта.
// Создайте геттеры и сеттеры для этих свойств.
// Создайте класс Programmer, который будет наследоваться от класса Employee, 
// и у которого будет свойство lang (список языков).
// Для класса Programmer перезапишите геттер для свойства salary. 
// Пусть он возвращает свойство salary, умноженное на 3.
// Создайте несколько экземпляров обьекта Programmer, выведите их в консоль.

class Employee {
    constructor(name, age, salary) {
        this._name = name
        this._age = age
        this._salary = salary
    }

    get name() {
        return this._name;
    }

    get age() {
        return this._age;
    }

    get salary() {
        return this._salary;
    }

    set name(newName) {
        this._name = newName;
    }

    set age(newAge) {
        this._age = newAge;
    }

    set salary(newSalary) {
        this._salary = newSalary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);

        this.lang = lang;
    }

    get salary() {
        return super.salary * 3
    }
}

const humanResources = new Employee('Artem', 28, 5000);
console.log(humanResources);

console.log(humanResources.salary);

const programmer_1 = new Programmer('Royce', 55, 10000, 'Brazillian');
console.log(programmer_1);

const programmer_2 = new Programmer('Dana', 58, 3000000, 'American')
console.log(programmer_2);

const programmer_3 = new Programmer('Paulo', 29, 80000, 'Brazillian');
console.log(programmer_3);

console.log(programmer_1.salary);
console.log(programmer_2.salary);

// У объектов есть скрытое свойство prototype, которое ссылается 
// на их прототипы. То есть, при вызове метода/свойства на объекте, если таковы отсутствуют, 
// то объект будет искать их в цепочке _proto_.

