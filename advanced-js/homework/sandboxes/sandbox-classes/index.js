

// const animal2 = {
//     name: 'Animal',
//     age: 5,
//     hasTail: true,
// };

// console.log(animal2);

class Animal {
    // Статические методы и переменные доступны только у самого класса
    // глобально в прототипах статический метод или переменную - не найти.
    static type = 'ANIMAL';
    constructor(options){
        this.name = options.name
        this.age = options.age
        this.hasTail = options.hasTail
    };
    voice() {
        console.log(`I'm Animal`);
    }
}
// Выведем значение статической переменной:
console.log(Animal.type);


// extends используется для создания дочернего класса 
// для уже существующего класса или встроенного объекта
class Cat extends Animal {
    static type = 'CAT'

    constructor(options) {
        // super - позволяет пользоваться методами родительского класса
        super(options)
        this.color = options.color
    }

    voice() {
        super.voice() // I'm Animal
        console.log(`I'm cat`); // I'm cat
    }
    // Геттер:
    get ageInfo() {
        return this.age * 7
    }
    // Сеттер
    set  ageInfo(newAge) {
        this.age = newAge
    }

}
const cat = new Cat({
    name: 'Cat',
    age: 7,
    hasTail: true,
    color: 'black'
});
console.log(cat);
console.log(cat.name);
console.log(cat.voice());
console.log(cat.ageInfo);
console.log(cat.ageInfo = 8);
console.log(cat.ageInfo);

console.log('========================');


class Component {
    constructor(selector) {
        this.$el = document.querySelector(selector)
    }
    hide() {
        this.$el.style.display = 'none'
    }
    show() {
        this.$el.style.display = 'block'
    }
}
// Box будет наследоваться от Component:
class Box extends Component {
    constructor(options) {
        super(options.selector)
        this.$el.style.width = this.$el.style.height = options.size + 'px';
        this.$el.style.background = options.color;
    }
}

const box1 = new Box({
    selector: '#box1',
    size: 100,
    color: 'red',
})
// Можем пользоваться методами для элемента show() и hide()
// тем самым скрывая или показывая элемент
// box1.show()
// box1.hide()

const box2 = new Box({
    selector: '#box2',
    size: 120,
    color: 'green',
})
// Так же, можем пользоваться этими методами для нового box2
// box2.hide()

class Circle extends Box {
    constructor(options) {
        super(options)

        this.$el.style.borderRadius = '50px'
    }
}
const c = new Circle({
    selector: '#circle',
    size: 90,
    color: 'blue'
})