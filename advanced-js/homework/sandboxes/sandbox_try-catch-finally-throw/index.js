

let res = 0;
try {
    res = 5/d;
    console.log(res);
}
catch(error) {
    console.log(error);
    console.log(error.name);
    console.log(error.message);
    console.log(error.stack);
}

// =================

let flSend = false;
try {
    if (!flSend) {
        flSend = true;
        // Отправка запроса к серверу
    }
}
catch (error) {
    console.log('Ошибка обработки запроса');
}
// Выполнится независимо от результата try/catch
finally {
    flSend = false;
}
console.log(flSend);

// =================
//trow
// Error, SyntaxError, TypeError, ReferenceError

function divide(a, b) {
    if (b === 0) {
        throw new Error('Делелие на ноль');
    }
    return a / b;
}

let ress = 0;
try {
    ress = divide(1, 2);
    console.log(ress);
}
catch (error) {
    if (error.name === 'Error') {
        console.log(error.name);
        console.log(error.message);
    } 
    else {
        throw error;
    }

}

