

function createCalfFunction(n) {
    return function() {
        console.log(1000 * n);
    }
}

const calc  = createCalfFunction(42);
calc(); // 42000


function craeteIncrementor(n) {
    return function(num) {
        return n + num;
    }
}
const addOne = craeteIncrementor(1)
const addTen = craeteIncrementor(10)

console.log(addOne(10));
console.log(addOne(40));

console.log(addTen(10));
console.log(addTen(40));


function urlGenerator(domain) {
    return function(url) {
        return `https://${url}.${domain}`
    }
}

const comUrl = urlGenerator('com');
const ruUrl = urlGenerator('ru');

console.log(comUrl('google')); // https://google.com
console.log(comUrl('netflex')); // https://netflex.com

console.log(ruUrl('vkontakte')); //  https://vkontakte.ru
console.log(ruUrl('yandex'));  //yandex.ru


// Напишем свою функцию bind()
function logPerson() {
    console.log(`Person: ${this.name}, ${this.age}, ${this.job}`);
}

const person1 = {name: 'Artem', age: 28, job: 'Frontend'};
const person2 = {name: 'Daria', age: 27, job: 'Designer'};

function bind(context, fn){
    return function(...args) {
        fn.apply(context, args)
    }
}

bind(person1, logPerson)(); // Person: Artem, 28, Frontend
bind(person2, logPerson)(); // Person: Daria, 27, Designer

// ====================================

function t1() {
    let a = 0;
    return function () {
        a = a + 1;
        return a;
    }
}
let b = t1();

console.log(b()); // 1
console.log(b()); // 2
console.log(b()); // 3
console.log(b()); // 4






