console.log(`INDEX 4 ================`);

const p7 = {
    name: 'P7',
    doSomething: function(param1, param2) {
        console.log(`Executing for object with name => ${this.name}`);
        console.log(param1, param2);
        return true;
    }
};

const p8 = {
    name: 'P8',
    doSomethingForP6: function() {
        console.log(`Method in P8 object -> ${this.name}`);
    }
};

// bind - создает новую функцию и возвращает в переменную
const newFunction = p7.doSomething.bind(p8);
newFunction('param1', 'param2');
