
console.log('INDEX 3 ===================');

const p5 = {
    name: 'P5',
    doSomething: function(param1, param2) {
        console.log(`Executing for object with name => ${this.name}`);
        console.log(param1, param2);
        return true;
    }
};

const p6 = {
    name: 'P6',
    doSomethingForP6: function() {
        console.log(`Method in P6 object -> ${this.name}`);
    }
};

// apply принимает аргеметы в виде массива:
p5.doSomething.apply(p6, [123, 345]);
// call примимает их списком через запятую:
p5.doSomething.call(p6, 123, 345);
// Оба метода возвращают результат выполенния функции


