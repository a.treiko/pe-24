// Передадим динамическим путем метод:
const methodName = 'doSomething';

const p1 = {
    name: 'P1',
    doSomething: function() {
        console.log(`Executing for object with name => ${this.name}`);
    }
};

const p2 = {
    name: 'P2',
    doSomethingForP2: function(param) {
        console.log(`Method in P2 object -> ${this.name}, ${param}`);
    }
};

p1.doSomething();
p2.doSomethingForP2();

p1.doSomething.call(p2);
p2.doSomethingForP2.call(p1);

p2.doSomethingForP2(111); // Передаем param

// Вызвать динамическим путем:
p1[methodName].call(p2);
