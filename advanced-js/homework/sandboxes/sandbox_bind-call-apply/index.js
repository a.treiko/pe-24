

function Person({name, age, height}) {
    this.person_name = name;
    this.person_age = age;
    this.person_height = height;
}

const p = new Person({
    name: 'Artem',
    age: 28,
    height: 171,
});

const p3 = {
    person_name: 'Michel',
    person_age: 22,
    person_height: 180,
}

console.log('p ->', p);
console.log(p3);
console.log(p.person_name);
console.log(p.person_age);
console.log(p.person_height);

const arr = [
    new Person({
        name: 'Daria',
        age: 28,
        height: 161,
    }),
    new Person({
        name: 'Zina',
        age: 27,
        height: 169,
    }),
    new Person({
        name: 'Nastya',
        age: 29,
        height: 175,
    }),
];

arr.forEach(person => {
    console.log(`${person.person_name}, ${person.person_age}`);
});







