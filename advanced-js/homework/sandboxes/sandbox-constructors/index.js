

const john = {
    name: 'John',
    sales: 10,
    sell: function(thing) {
        this.sales += 1;
        return 'Manager ' + this.name + ' sold ' + thing;
    }
};

console.log(john.sales);
console.log(john.sell('Apple'));
console.log(john.sell('Pomegrade'));
console.log(john.sales);

const mary = {
    name: 'Mary',
    sales: 120,
    sell: function(thing) {
        this.sales += 1;
        return 'Manager ' + this.name + ' sold ' + thing;
    }
};

console.log(john.sell === mary.sell);


const manager = function(name, sales) {
    return {
        name: name,
        sales: sales,
        sell: function(thing) {
            this.sales += 1;
            return 'Manager ' + this.name + ' sold' + thing;
        }
    }
}

const victor = manager('Victor', 34);
const paul = manager('Paul', 55);

victor.sell('pear');
paul.sell('watermelon');

console.log(victor.sales);
console.log(paul.sales);


const Manager = function(name, sales) {
    this.name = name;
    this.sales = sales;
    this.sell = function(thing) {
        this.sales += 1;
        return 'Manager ' + this.name + ' sold ' + thing;
    }
}

const daria = new Manager('Daria', 100);
const artem = new Manager('Artem', 99);

console.log(artem.constructor);
console.log(artem.constructor.name);
console.log(artem instanceof Manager);

function add(c, d) {
    console.log(this.a + this.b + c + d);
}

add(3, 4); // NaN

let ten = {a: 1, b: 2};
add.call(ten, 3, 4) // 10

add.apply(ten, [3, 4]);

