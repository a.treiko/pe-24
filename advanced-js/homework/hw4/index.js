// Обьясните своими словами, что такое AJAX и чем он полезен при разработке на Javascript.

// Задание
// Получить список фильмов серии Звездные войны, и вывести на экран список персонажей для каждого из них.

// Технические требования:

// Отправить AJAX запрос по адресу https://swapi.dev/api/films/ и получить список всех фильмов серии Звездные войны

// Для каждого фильма получить с сервера список персонажей, которые были показаны в данном фильме. 
// Список персонажей можно получить из свойства characters.
// Как только с сервера будет получена информация о фильмах, сразу же вывести список всех фильмов на экран. 
// Необходимо указать номер эпизода, название фильма, а также короткое содержание (поля episode_id, title и opening_crawl).
// Как только с сервера будет получена информация о персонажах какого-либо фильма, вывести эту информацию на экран под названием фильма.


// Необязательное задание продвинутой сложности
// Пока загружаются персонажи фильма, прокручивать под именем фильма анимацию загрузки. Анимацию можно использовать любую. Желательно найти вариант на чистом CSS без использования JavaScript.



const btn = document.getElementById('getBtn')
const getSection = document.querySelector('.get')

const ul = document.createElement('ul')
ul.classList.add('get-list')

btn.addEventListener('click', getList)

function getList(event) {
    event.preventDefault()
    fetch('https://swapi.dev/api/films/')
        .then(response => {
            // console.log(response);
            // console.log(response.json());
            return response.json()
        })
        .then(result => {
            // Массив объектов:
            const movies = result.results
            // console.log(movies);

            movies.forEach(el => {

                let {title, characters, episode_id, opening_crawl} = el

                let film = document.createElement('li')
                let filmTitle = document.createElement('h4')
                filmTitle.textContent = `${episode_id} : ${title}`
                let filmContent = document.createElement('p')
                filmContent.textContent = opening_crawl
                film.append(filmTitle, filmContent)

                characters.forEach(url => {
                    fetch(url)
                        .then(u => u.json())
                        .then(info => {
                            console.log(info);
                            let person = document.createElement('ul')
                            for (let key in info) {
                                let personProp = document.createElement('li')
                                personProp.classList.add('person-item')
                                personProp.textContent = `${key} : ${info[key]}`
                                person.append(personProp)
                                // console.log(personProp);
                            }
                            film.append(person)
                        })
                })
                ul.append(film)
            });
            btn.remove()
            getSection.append(ul)
        })
}

// AJAX - асинхронный js код. С помощью которого,
// мы можем удобно структурировать и строить пользовательский интерфейс веб-приложения.
