// Обьясните своими словами, как вы понимаете асинхронность в Javascript

// Задание
// Написать программу "Я тебя по айпи вычислю"

// Технические требования:

// Создать простую HTML страницу с кнопкой Вычислить по IP.
// По нажатию на кнопку - отправить AJAX запрос по адресу https://api.ipify.org/?format=json, получить оттуда IP адрес клиента.
// Узнав IP адрес, отправить запрос на сервис https://ip-api.com/ и получить информацию о физическом адресе.
// Под кнопкой вывести на страницу информацию, полученную из последнего запроса - континент, страна, регион, город, район города.
// Все запросы на сервер необходимо выполнить с помощью async await.

const section = document.querySelector('.finder')
const btn = document.querySelector('#btn')
const ul = document.createElement('ul')

btn.addEventListener('click', async() => {
    const url = 'https://api.ipify.org/?format=json'
    const responsedId = await axios.get(url)
    // console.log(responsedId)
    const {data} = responsedId
    // console.log(data)
    const ip = data.ip
    // console.log(data.ip);
    console.log(ip);
    const urlAddress = `https://ip-api.com/${ip}`
    console.log(urlAddress);
    const info = await axios.get(urlAddress)
    const {timezone, country, region, city, zip} = info.data
   

})