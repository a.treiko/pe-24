
// Task

// - Разрабоать модель взаимодействия в группе,
// например для Товаров.
// - Сделать класс Group с свойствами:
//     - title
//     - descriprion
//     - _items
//     - subtitle
//     - характеристика группы с количеством элементов
//     добавленных к группе, например " LCD панели. Найдено 5 шт"
// - В группе розработать метод addItem и deleteItem
// - На уровне с классом Group разработать класс, который имел бы базовые 
//     свойства для элементов группы, например title, createAt и updated_at
// - Создать класс Product, который наследует Item и определяет дополнительное 
//     свойсво price, которое должно использоваться в ГЕТТЕР!
// - Создать 2 группы, например "Мониторы" и "Телевизоры". 
//     В первую группу добавить 3 продукта (заранее создать их), а 
//     во вторую группу - другие два.
// - Вызвать alert(group) и alert(group2). И данный синтаксис, должен
//     вывести в описание группы, например -> "LCD панели. Найдено 5 шт"

class Group {
    constructor(title, description, items = []) {
        this.title = title;
        this.description = description;
        this._items = items;
    }

    addItem(item) {
        this._items.push(item);
        return this;
    }

    toString() {
        return `${this.title}, ${this.description} ---> (${this._items.length})`;
    }
    // @desc delete item
    // @param {Number} id
    // @return {boolean}
    deleteItemById(id) {
        const index = this._items.findIndex((i) => {
            return i.id === id;
        });
        if (index >= 0) {
            this._items.splice(index, 1);
            return true;
        }
        return false;
    }

    get subtitle() {
        return `${this.title} (${this._items.length})`
    }
}

class Item {
    constructor(title) {
        this._id = ++Item.ID_INCREASER;
        this.title = title;
        this.created_at = new Date();
        this.updated_at = new Date();
    }
    get id() {
        return this._id;
    }
}
Item.ID_INCREASER = 0;

class Product extends Item {
    // @param {object} options
    // @property {string} title
    // @property {number} price
    constructor(options) {
        const {title, price} = options;
        super(title);
        this.price = price;
    }
    get price() {
        return `${this._price.toFixed(2)} UAH`;
    }

    set price(value) {
        if (value < 0) {
            throw new Error(`The price is incorrect`);
        }
        this._price = +value;
    }
}

// GROUPS:
const group_1 = new Group('TV', 'Black');
group_1.addItem(new Product({title: 'LG 3310',price: 2000}))
        .addItem(new Product({title: 'Samsung Max 228', price: 5000}))
        .addItem(new Product({title: 'IPhone 12 Pro', price: 8000}));

const group_2 = new Group('PC', 'White');
group_2.addItem(new Product({title: 'Lenovo 47', price: 12300}))
        .addItem(new Product({title: 'IMac Pro', price: 30000}));

alert(group_1)
alert(group_2)










// =============================================
// class Group {
//     constructor(title, description, items = []) {
//         this.title = title;
//         this.description = description;
//         this._items = items
//     }

//     addItem(item) {
//         this._items.push(item);
//     }

// // @desc delete
// // @param {item} item
// // @return {boolean}

//     deleteItem() {
//         const index = this._items.findIndex((i) => {
//             return i.id === item.id
//         })
//         if (index >= 0) {
//             this._items.splice(index, 1);
//             return true;
//         }
//         return false;
//     }

//     get subtitle() {
//         return `${this.title}. (${this._items.length})`
//     }
// }

// class Item {
//     constructor(title) {
//         this._id = ++Item.ID_INCREASER;
//         this.title = title;
//         this.created_at = new Date();
//         this.updated_at = new Date();
//     }
// }
// Item.ID_INCREASER = 0;


// class Product extends Item {
//     /*
//     @ param {object} options
//      @property {string} title
//      @property {number} price
//     */ 
//     constructor(options) {
//         const {title, price} = options;
//         super(title);
//         this.price = price;
//     }

//     get price() {
//         return `${this._price.toFixed(2)} uah`
//     }

//     set price(value) {
//         if (value < 0) {
//             throw new Error(`The price os incorrect`)
//         }
//         this._price = +value;
//     }
// }

