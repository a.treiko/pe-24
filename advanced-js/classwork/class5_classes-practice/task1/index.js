
function BaseFuncTask(title) {
    this.title = title;
    this.done = false;
}

BaseFuncTask.prototype.complete = function() {
    this.done = true;
}

function TaskFunc(title) {
    BaseFuncTask.call(this, title);
}
TaskFunc.prototype = Object.create(BaseFuncTask.prototype);



class BaseClass {
    constructor(title) {
        this.title = title;
        this.done = false;
    }
    complete() {
        this.done = true;
    }
}

class Task extends BaseClass {
    constructor(title) {
        super(title);
    }
}

class NextGeneration extends Task {
    constructor(title) {
        super(title);
    }
}

const newObj = new TaskFunc('Some title for the task!');
const obj2 = new Task('Another description');

obj2.complete();




