class Task {
    constructor(title, _done = false) {
        this.title = title;
        this._done = _done;
        this._estimation = 0;
    }

    get done() {
        return this._done ? 'Сделано' : 'Не сделано';
    }

    set done(val) {
        this._done = val;
    }

    set estimation(val) {
        if (val < 0 || val > 12) {
            throw new Error('The estimation sould be exactly in certaiin interval 0-12');
        }
        this._estimation = val;
    }
}

// const o = new Task('Сделать домашку')
// undefined
// o.done
// "Не сделано"
// o.done = true
// true
// o.done
// "Сделано"

const o = new Task('Создать домашку');

for (const field in o) {
    console.log(`${field} --> ${o[field]}`);
}