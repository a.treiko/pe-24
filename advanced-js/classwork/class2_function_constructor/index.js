
function constructorImitation(name, age) {
    return  {
        name,
        age,
        parent: {
            dadName: 'Alex',
            motherName: 'Victoria',
            getParrents: function() {
                console.log(`${this.dadName}, ${this.motherName}`);
            }
        },
        sayHi() {
            console.log(`Hello, ${this.name}`);
        }
    };
}

function getParrents() {
    console.log(`${this.dadName}, ${this.motherName}`);
}

const user = constructorImitation("Artem", 28);
console.log(user)

function User (name, age) {
    this.name = name;
    this.age = age;
    this.sayHi = function() {
        console.log(`Hello, ${this.name}`);
    }
}

const user2 = new User('Daria', 27);
console.log(user2);

user2.sayHi();
// Меняет контекст:
user.sayHi.call(user2);
// Меняет контекст и возвращает новую функцию с измененным контекстом:
user.sayHi.bind(user2)();



