
// =========== Task1 ===========
// Напишите функцию конструктор, созжающую объект, реализуюющий такой функционал:
// у нас на стронице есть вопрос: При первом клике на него под ним открывается ответ на вопрос.
// При повторном прячется. 

// <div id="root"></div>

    const questionsText = "Девиз дома Баратеонов";
    const questionsAnswer = "Нам ярость!";

const link = document.querySelector('.question');

function Questions(link, questionsAnswer) {
    this.flag = false;
    this.checkFlag = function() {
        this.flag = !this.flag;
    }
        this.answerToggle = function() {
            link.addEventListener('click', this.linkClickHandler)
        }
        this.linkClickHandler = function(event) {
            event.preventDefault();
            if (this.flag) {
                answer.remove();
            } else {
                let answer = document.createElement('p');
                answer.textContent = questionsAnswer;
                console.log(questionsAnswer);
                let root = document.getElementById('root');
                root.append(answer);
            }
            this.checkFlag();
        }
}

const question = new Questions(link, questionsAnswer);
question.answerToggle();


// =========== Task2 ===========
// Создайте функцию-конструтор объекта stopwatch согласно описанию: 1.
// Свойства объекта: - _time: время; - container: ссылка на DOM-элемент,
// внутри которого нужно выводить время. 1. Методы объекта: - start, stop,
// reset, работающие с его свойствами. - setTime и getTime. setTime
// будет получать новое значение времени в качестве аргумента, и проверять,
// является ли оно положительным целым числом. Если да - то свойству _time
// будет присвоено значение аргумента, переданного в метод setTime и метод
// вернет ответ объект вида: { status: "success", }

// Если же аргумент не удовлетворяет критериям, то возвращать объект вида:

// {
// status: "error",
// message: "argument must be positive integer"
// }
// 
// Метод же `getTime` будет просто возвращать значение свойства `_time` для использования его вне методов объектов.

// <p id="time"></p>
// <button id="start-time">Start</button>
// <button id="stop-time">Stop</button>
// <button id="reset-time">Reset</button>


//    function Stopwatch(container) {

//    }

//    const startBtn = document.getElementById('start-time');
//    const stopBtn = document.getElementById('stop-time');
//    const resetBtn = document.getElementById('reset-time');

//    const stopWatchContainer = document.getElementById('time');
//    const stopwatch = new Stopwatch(stopWatchContainer);

//    startBtn.addEventListener('click', stopwatch.start.bind(stopwatch));
//    stopBtn.addEventListener('click', stopwatch.stop.bind(stopwatch));
//    resetBtn.addEventListener('click', stopwatch.reset.bind(stopwatch));

function Stopwatch(time, link) {
    this._time = time;
    this.container = link;
    this.start = function() {

    };

    this.stop = function() {

    };

    this.reset = function() {
        
    }

    this.setTime = function() {
        let status = 'Error';
        if (time > 0 && time % 1 === 0) {
            this._time = time;
            status = 'Success';
            return {status: status}
        }
        return {status: status, message: 'argument must be positive integer'};
    };

    this.getTime = function() {

        return this._time;
    };
}

const startBtn = document.getElementById('start-time');
const stopBtn = document.getElementById('stop-time');
const resetBtn = document.getElementById('reset-time');

const stopWatchContainer = document.getElementById('time');
const stopwatch = new Stopwatch(stopWatchContainer);

startBtn.addEventListener('click', stopwatch.start.bind(stopwatch));
stopBtn.addEventListener('click', stopwatch.stop.bind(stopwatch));
resetBtn.addEventListener('click', stopwatch.reset.bind(stopwatch));