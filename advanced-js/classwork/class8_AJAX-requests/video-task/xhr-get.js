

const requestURL = 'https://jsonplaceholder.typicode.com/users';
// Мы хотим сделать запрос по адресу
// и ловить ответ из сервера

function sendRequest(method, url) {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest()
        // Откроем соединение методом open
        // 1 параметр - метод, по которому мы будем делать запрос
        // 2 параметр - url 
        xhr.open('GET', requestURL)
        
        // Обработаем полученные данные перед отправкой запроса:
        // console.log(xhr.response) - выведет полученные данные в виде строки
        
        // Чтоб распарсить данные с строки в объекты - JSON.parse
        // console.log(JSON.parse(xhr.response))
        
        // Так же, распарсить можно с помощью:
        xhr.responseType = 'json';
        
        xhr.onload = () => {
            if (xhr.status >= 400) {
                reject(xhr.response);
            } else {
                resolve(xhr.response);
            }
        }
        // Отлавливаем ошибку:
        xhr.onerror = () => {
            reject(xhr.response);
        }
        // Отправляем запрос:
        xhr.send()
    })
}

// Так как sendRequest возвращает промис - используем then
sendRequest('GET', requestURL)
    .then(data => console.log(data))
    .catch(err => console.log(err))





