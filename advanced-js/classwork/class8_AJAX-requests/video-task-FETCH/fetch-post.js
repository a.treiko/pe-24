
const requestURL = 'https://jsonplaceholder.typicode.com/users'

function sendRequest(method, url, body = null) {
    const headers = {
        'Content-Type': 'applecation/json'
    };

    return fetch(url, {
        method: method,
        body: JSON.stringify(body),
        headers: headers
    })
        .then(response => {
            // Если мы попали в then и тут ошибка:
            if (response.ok) {
                return response.json();
            }
            return response.json()
                .then(error => {
                    const e = new Error('Что-то пошло не так');
                    e.data = error;
                    throw e
                })
        })
}

const body = {
  name: 'Artem',
  age: 28
}

sendRequest('POST', requestURL, body)
  .then(data => console.log(data))
  .catch(err => console.log(err))