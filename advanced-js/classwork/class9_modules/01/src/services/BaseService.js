import axios from 'axios';

export const MAX_TIMEOUT = 500;

export default class BaseService {
	constructor() {
	}

	/**
	 * @descr
	 **/
	static async requestGet(url) {
		return axios.get(url);
	}
}