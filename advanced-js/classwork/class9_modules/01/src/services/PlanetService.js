import Base, {MAX_TIMEOUT} from './BaseService';

export default class PlanetService extends Base {
	constructor() {
		super();
	}

	static async getPlanets() {
		return PlanetService.requestGet('https://swapi.dev/api/planets/');
	}
}