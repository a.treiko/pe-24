// import {toISOFormat} from "./utils/dateAndTime";

import PlanetService from "./services/PlanetService";
import {Planet} from "./models/Planet";

PlanetService
	.getPlanets()
	.then(({data}) => {
		console.log('Result --> ', data);

		document.write(
			data.results.map(
				planet => `Planet ==> ${new Planet(planet).toHTML()}`
			).join('')
		);

	});

// console.log(toISOFormat(Date.now()));