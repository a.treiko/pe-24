/**
 *
 **/
export const toISOFormat = str => {
	return new Date(str).toISOString();
}