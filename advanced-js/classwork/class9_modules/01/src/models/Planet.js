export class Planet {
	constructor(props) {
		// this is good!!!!
		const {
			climate,
			created,
			edited,
			films,
			gravity,
			name
		} = props;

		this.climate = climate;
		this.created = created;
		this.edited = edited;
		this.films = films;
		this.gravity = gravity;
		this.name = name;

		////// Not very clear for understanding /////
		// for(let prop in props) {
		// 	this[prop] = props[prop];
		// }
	}

	toHTML() {
		return `<ul><li><strong>climate:</strong> ${this.climate}</li></ul>`;
	}
}