



function onBtnClick() {
    // fetch - вернет объект Promise
    fetch('https://swapi.dev/api/people/')
        .then(function(response) {
            return response.json();
        })
        .then(function(response) {
            console.log(response);
        })
        .catch(e => console.error)
        .finally(function() {
            console.log('Request is completed!');
        })
}



