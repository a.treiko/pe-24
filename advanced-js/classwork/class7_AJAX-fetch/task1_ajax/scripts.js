
// Создать страницу, которая при загрузке будет
// формировать список рабочих компании "X"

// Список должен быть выведен в 'ul' и 'li'.
// Каждый элемент списка можно удалить (С ДОМ)
// и получить детали. Создать кнопку "Детали" и "Удалить"
// Для деталей мы отправляем дополнительный запрос
// на url '/people/:id/'
// Для удаления с ДОМ, просто - удаляем элемент из
// списка как ДОМэлемент

// Для работы с AJAX запросами создать класс и метод 
// get для получения GET запроса. 
// Данный метод должен возвращать Promise 
// в котором будут обработаны кейсы удачной загрузки
// и появлений ошибок.

document.addEventListener('DOMContentLoaded', function () {
	const ul = document.createElement("ul");

	ul.innerHTML = '<li>Loading...</li>';
	ul.setAttribute('id', 'ul-wrapper')
	ul.onclick = onPersonBtnClick;
	document.body.append(ul)

	PersonService.getPersons()
		.then(function (response) {
			ul.innerHTML = response.results
				.map(function(item, index) {
					return `<li>${new Person(item)}
                    <button>Delete</button>
                    </li>`
				})
				.join('');
			console.log(response);
		})
});



function onPersonBtnClick(event) {
	const element = event.target;
	if (element.tagName === 'BUTTON'){
		element.closest('li').remove();
	}
}

