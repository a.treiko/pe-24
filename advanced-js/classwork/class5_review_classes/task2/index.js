

// Возьмите код класса Modal и с его помощью создайте универсальных класс, который
// отвечает за каркас всплывающего окна, и на его основе 2 других:

// Вплывающее окно с формой регистрации. Его HTML-разметка будет выглядть так:

// ×
// 2. Всплывающее окно с формой авторизации. Его HTML-разметка:
// ×

// Привяжите открытие первого окна к кнопке Регистрация, а второго - к кнопке Вход
// Регистрация
// Вход

// body {
// font-family: Arial, Helvetica, sans-serif;
// }


// .modal {
// display: none;
// position: fixed;
// z-index: 1;
// padding-top: 100px;
// left: 50%;
// top: 50%;
// transform: translate(-50%, -50%);
// width: 200px;
// overflow: auto;
// background-color: rgb(0, 0, 0);
// background-color: rgba(0, 0, 0, 0.4);
// background-color: #fefefe;
// padding: 20px;
// border: 1px solid #888;
// }

// .modal.active {
// display: block;
// }

// .close {
// color: #aaaaaa;
// float: right;
// font-size: 28px;
// font-weight: bold;
// }

// .close:hover,
// .close:focus {
// color: #000;
// text-decoration: none;
// cursor: pointer;
// }

const btnReg = document.getElementById('open-register-modal')
const btnLog = document.getElementById('open-login-modal')


class RegisterModal {
    constructor(id, className) {
        this.id = id;
        this.className = className;
        this.wrapper = this.render();
    }
    render() {
        const wrapper = document.createElement('div');
        wrapper.classList.add(...this.className);
        wrapper.id = this.id;
        const modal = document.createElement('div');
        modal.className = 'modal-content';
        const span = document.createElement('span');
        span.className = 'close';
        span.innerHTML = '&times;';
        const form = document.createElement('form');
        form.id = 'register-form';
        form.setAttribute('action', '#');
        this.closeBtn = span;
        const login = this._createInput('text', 'login', 'Ваш логин', true);
        const email = this._createInput('email', 'email', 'Ваш email', true);
        const password = this._createInput('password', 'password', 'Ваш пароль', true);
        const repeatPassword = this._createInput('password', 'password', 'Повторите пароль', true);
        const submitBtn = document.createElement('input');
        submitBtn.setAttribute('type', 'submit');
        submitBtn.setAttribute('value', 'Регистрация');
        
        form.append(login, email, password, repeatPassword, submitBtn);
        modal.append(span, form);
        wrapper.append(modal);

        return wrapper;
    }
    _createInput(type, name, placeholder, required) {
        const input = document.createElement('input');
        input.setAttribute('type', type);
        input.setAttribute('name', name);
        input.setAttribute('placeholder', placeholder);
        input.setAttribute('required', required);
        if (required) {
            input.setAttribute('required', required)
        }
        return input;
    }
    toggleClass(className) {
        this.wrapper.classList.toggle(className);
    }
    closeBtnHandler() {
        this.addEventListener('click', () => {
            this.toggleClass('active');
        })
    }
}
let reg = new RegisterModal ('registration', ['class1', 'class2', 'modal']);
reg.closeBtnHandler();
let root = document.getElementById('root');
btnReg.addEventListener('click', function() {
    root.append(reg.wrapper);
    reg.toggleClass('active');
    
})

btnLog.addEventListener('click', function() {
    
})
class LoginModal {
    constructor(id, className) {
        this.id = id;
        this.className = className;
    }
}

