// TASK 1
// Напишите класс Пациент с такими параметрами:

// ФИО;
// дата рождения;
// пол;

// И на его основе создайте 2 расширенных класса:

// ПациентКардиолога, с дополнительными параметрами:


// среднее давление;
// перенесенные проблемы с сердечно-сосудистой системой;


// ПациентСтоматолога с дополнительными параметрами:


// дата последнего визита;
// текущее лечение; 

class Patient {
    constructor(fullName, birthDate, gender) {
        this.fullName = fullName;
        this.birthDate = birthDate;
        this.gender = gender;
    }
}

class CardioPatient extends Patient {
    constructor(fullName, birthDate, gender, averagePressure, cardioProblems) {
        super(fullName, birthDate, gender)
        this.averagePressure = averagePressure;
        this.cardioProblems = cardioProblems;
    }
}

class DantelPatient extends Patient {
    constructor(fullName, birthDate, gender, visitDate, currentTreatment) {
        super(fullName, birthDate, gender)
        this.visitDate = visitDate;
        this.currentTreatment = currentTreatment;
    }
}

const patient = new CardioPatient('Artem', '25.06.92', 'male', '90/115', 'none');
console.log(patient);

const patient2 = new DantelPatient('Daria', 'Babii', 'female', '20.09.20', 'none')
console.log(patient2);

