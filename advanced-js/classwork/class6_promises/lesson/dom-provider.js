class DomProvider {
    constructor(el = null) {
        this._element = el && this.createElement(el) || null;
    }

    get element() {
        return this._element;
    }
        
    createElement(el) {
        return document.createElement(el);
    }    

    getById(id) {
        return document.getElementById(id);
    }
}