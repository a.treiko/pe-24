class TableGame extends DomProvider {
    constructor(el) {
        super('div');
        this._hostElement = this.getById(el);
    }

    get wrapper() {
        return this._hostElement;
    }

    start() {
        const table = new Table(3, 3);

        this.wrapper.appendChild(table.build());

    }
}