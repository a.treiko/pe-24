class Table extends DomProvider {
    constructor(col = 10, row = 10) {
        super('table');

        this.interval = null;
        this.element.setAttribute('border', '1');

        this.col = col;
        this.row = row;
    }

    build() {
        if(this.interval) {
            clearInterval(this.interval);
        }
        // this.element
        for (let i = 0; i < this.row; i++) {
            const row = this.addRow();

            for (let j = 0; j < this.col; j++) {
                row.addCell();
            }

            this._element.appendChild(row.element);
        }

        this.interval = setInterval(function() {
            const randomRow = Math.floor(Math.random() * this.row);
            const randomCell = Math.floor(Math.random() * this.col);

            const row = this.element.children.item(randomRow);
            const cell = row.children.item(randomCell);

            cell.style.backgroundColor = 'red'

            // console.log(this.element.children);

        }.bind(this), 2000);

        return this.element;
    }

    addRow() {
        return new Row();
    }
}