class Row extends DomProvider {
    constructor() {
        super('tr');
    }

    addCell() {
        const cell = new Cell();

        this.element.appendChild(cell.element);

        return cell;
    }
}