
// Разработать класс, которая принимает URL на изображение,
// и возвращает Promise с результатом вызова 
// или генерирует ошибку. Реализация на классах

const body = document.querySelector('body');

class Loader {
    constructor() {
        this._status = null;
    }
    load(url) {
        return new Promise((resolve, reject) => {
            let img = document.createElement('img');
            img.src = url;
            img.onload = function() {
                resolve(img);
            }

            img.onerror = function(e) {
                reject(e);
            }
        })
    }
}

const loader = new Loader();
loader.load('https://images.theconversation.com/files/350865/original/file-20200803-24-50u91u.jpg?ixlib=rb-1.1.0&rect=37%2C29%2C4955%2C3293&q=45&auto=format&w=926&fit=clip')
    .then((el) => {
        body.appendChild(el);
    })
    .catch((e) => {
        console.log('Error -->', e);
    });