


function first() {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve({title: 'First article', viewers: 1234});
        }, 2000)
    })
}

function second() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve([{title: 'Second article', viewers: 12}]);
        }, 500)
    })
}

// функция all ждет выполнения всех функций внутри нее и возвращает
// результат выполненных функций в коллекции, которую можем перебрать

// any - противоположная функции all
// она вернет результат первой выполненной фунции.
Promise
    .any([
        first(),
        second(),
    ])
    .then(function(val){
        console.log('Two functions were successfully executed!', val)
    })
    .catch(e => {
        console.log('Error -->', e);
    })

