

function onSomeRequestHandler(val) {
    console.log('Resolved --->', val);
    
    return new Promise(function(resolve) {
        setTimeout(function() {
            resolve('again new value!');
        }, 1500)
    });
};

function onSomeOtherRequestsHandler(val2) {
    console.log('Value from previous Promise --->', val2);
}

console.log('Start operation...');

const loadPicture = new Promise(
    function (resolve, reject) {

        setTimeout(() => {
            resolve('NewValueForPromiseResult');
        }, 1500)
    }
);

loadPicture
    .then(onSomeRequestHandler)
    .then(onSomeOtherRequestsHandler);



