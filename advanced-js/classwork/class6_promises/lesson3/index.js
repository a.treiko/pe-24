
const p = new Promise(function(resolve, reject) {
    setTimeout(() => {

        // Генерируем рандомное число
        const randNumber = Math.random() * 100;

        
        if (randNumber < 50) {
            // Отлавливаем ошбку в reject если < 50
            reject(new Error('Something very bad has just happened'))
        } else {
            // Иначе добавим объект с свойством guessNumber и его сгенерированный номер
            resolve({
                guessNumber: randNumber,
            })
        }
    }, 1500)
})


// then принимает в себя две функции:
// - для обработки успешных событий
// - для обработки ошибки
// p.then(function(){}, function(error){
//     console.log('!!! --->', error, '!!!!!!');
// })

p   // num - наше число сгенерированное и переданное в resolve 
    .then(function(num) {
        console.log('!!! ----> YOU ARE LUCKY', num);
        return new Promise(function(resolve, reject) {
            resolve(num.guessNumber + Math.random() * 1000);
        });
    })
    
    // Запустим следующий then с сгенерированным числом
    .then(function(val){
        console.log('Final version of our number is -->', val);
        return 'Special force!';
    })
    
    // Отлавливаем ошибку с reject в catch
    // Когда случаеся catch - ни в один then не заходит!
    .catch(e => {
        console.error(e);
        return e;
    })

    // Если после catch запустить еще один then - мы его поймаем
    .then(e2 => {
        console.log('E2 --->', e2);
    })






