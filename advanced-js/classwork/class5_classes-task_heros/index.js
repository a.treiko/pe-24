class Warrior {
    constructor(health = 1000, power = 100) {
        this.health = health;
        this.power = power;
        this._points = 0;
    }

    addPoits(points) {
        this._points += points;
        return this;
    }

    isAlive() {
        return this.health > 0;
    }

    decreaseHealth(points) {
        this.health -= points;
        return this;
    }
}

class Human extends Warrior {
    constructor() {
        super (800, 100);
    }
}
class Ork extends Warrior {
    constructor() {
        super (2000, 150);
    }
}
class Elf extends Warrior {
    constructor(health, power) {
        super (health, power);
    }
}
class WhiteElf extends Warrior {
    constructor() {
        super (500, 80);
    }
}
class BlackElf extends Warrior {
    constructor() {
        super (600, 90);
    }
}

