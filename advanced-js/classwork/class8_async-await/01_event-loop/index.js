// Что выведется в консоль?
// 1, 3, 6, 4, 5, 2
// * Приоритет промиса

console.log('1. hello');

setTimeout(() => {
    console.log('2. hello');
}, 300);

console.log('3. hello');

Promise
    .resolve({})
    .then(() => {
        console.log('4. hello');
    });

setTimeout(() => {
    console.log('5. hello');
}, 0);

console.log('6. hello');

// === next task
function a() {
    b();
    
    // for (let i = 0; i < 100; i++) {
    //     console.log('I --->', i);
    // }

    const arr = new Array(1000).fill(5);
    console.log(arr);

    arr.forEach(item => {
        console.log('Item ->', item);
    });
}

function b() {
    setTimeout(() => {
        console.log('Oooops!!!');
    }, 0)
}

a();
// 1, 3, 6, for, 4, 5, Oops!, 6

