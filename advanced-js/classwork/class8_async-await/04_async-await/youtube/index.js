
const delay = ms => {
    return new Promise(resolve => {
        // Если timeout завершится успешно - будем вызывать resolve с кол-ом ms, которые мы задали
        setTimeout(() => resolve(), ms)
    })
}

// Так как мы имеем доступ к промису - используем then
// delay(2000).then(() => console.log('2 sec'))

const url = 'https://jsonplaceholder.typicode.com/todos/1';

// function fetchTodos() {
//     // После каждого фетча будем выводить в консоль:
//     console.log('FETCHED');
//     // После задержки выполним callback
//     delay(2000).then(() => {
//         // fetch - делает асинхронный запрос и возвращает Promise
//         return fetch(url)
//     })  // Распарсим в нужный формат то, что пришло с сервера
//         .then(response => response.json())
// }

// // Теперь функция имеет then
// fetchTodos()
//     .then(data => {
//         console.log('Data =>', data)
//     })
//     .catch(e => console.log(e))


// Сделаем через async/await
// async работает только с асинхронными операциями
async function fetchAsyncTodos() {
    console.log('FETCHED');
    // await в try! 
    try {
    // Не перейдем на следующую операцию, пока код фукнция с await не получит resolve
    await delay(2000)
    // await помогает автоматически обработать promise
    // fetch возвращает response, который попадает в then
    // с await мы может получить response в переменную как результат работы await и fetch    
    const response = await fetch(url)
    const data = await response.json()
    console.log(data);
    } catch (e) {
        console.error(e);
    } finally {

    }
    

}
// Можем вызывать then:
fetchAsyncTodos()