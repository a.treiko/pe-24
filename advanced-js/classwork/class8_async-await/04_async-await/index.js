async function execGetQuery(url) {
    const response = await fetch(url);
    return response.json();
}

async function getFilms() {
    return await execGetQuery('https://swapi.dev/api/films/');
}

async function getCharacter(url) {
    return await execGetQuery(url);
}

const ul = document.createElement('ul');
document.body.append(ul);

getFilms()
    .then(({results}) => results)
    .then(films => {
        // // Деструктуризируем:
        // films.forEach(({characters}) => {
        //     characters.forEach((character) => {
        //         console.log(character);
        //     })
        //     console.log(characters);

        // });
        films
            .forEach(({title, characters}) => {
                const li = document.createElement('li');
                li.innerHTML = title;
                ul.appendChild(li);

                Promise
                    .all(characters.map(url => execGetQuery(url)))
                    .then((charactersList) => {
                        const cUl = document.createElement('ul');
                        li.appendChild(cUl);

                        charactersList.forEach(({name}) => {
                            const cLi = document.createElement('li');
                            cLi.innerHTML = name;
                            cUl.appendChild(cLi);
                        })
  
                    })
            })
    })

