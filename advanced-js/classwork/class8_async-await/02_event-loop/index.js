

function asyncFor(array, cb) {
    array.forEach(item => {
        setTimeout(function() {
            cb(item);
        }, 0)
    });
}

console.log('1. Before function');

asyncFor([1, 2, 3, 4, 5], function(i) {
    console.log('2. Output -->' , i);
})

console.log('3. After function');