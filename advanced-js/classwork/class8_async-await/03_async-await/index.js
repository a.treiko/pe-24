
function getSomeData() {
    return new Promise((resolve, reject) => {
        try {
            setTimeout(function() {
                const obj = {
                    title: 'some data'
                };
                resolve(obj);
            }, 1500)
        } catch (e) {
            reject(e);
        }
    })
}



// getSomeData()
// .then(result => {
//     console.log('Result ->', result);
// })
// .then()
// .then();

