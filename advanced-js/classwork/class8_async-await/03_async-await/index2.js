

async function asyncFunction() {
    const data = await getSomeData();

    console.log('Hello my dear friends', data);

    return 'Some very expected value!'
}

// Аналог функции без async-await:

function asyncFunctionNewVersion() {
    return new Promise((resolve, reject) => {
        getSomeData()
            .then(result => {
                resolve(result)
                console.log('Hello my dear friends', result);
                return 'Some very expected value!'
            })
            .catch(reject);
    })
}

asyncFunction()
    .then(res => {
        console.log('My result from async function ->', res);
    })``