// Создать прототип игровых персонажей

// Warrior
//     Орк
//     Человек
//     Эльф
//         Темный Эльф
//         Светлый Эльф

// Игровые персонажи могут бить.

// Человек
//     100 баллов
// Орк
//     200 баллов

// Игровой персонаж может бить одного игрового персонажа.
// При ударе, бьющий - получает 100 баллов, а получающий -
// указанное кол-во баллов.Warrior

// Каждый игровой персонаж имеет стартовое кол-во баллов.

// ================================================

function Warrior(health=1000, power = 100) {
	this.health = health;
	this._points = 0;
	this.power = power;
}

Warrior.prototype.addPoints = function(points){
	this._points += points;
	return this;
};

Warrior.prototype.isAlive = function(){
	return this.health > 0;
};

Warrior.prototype.decreaseHealth = function(points){
	this.health -= points;
	return this;
};

Warrior.prototype.hit = function (enemy) {
	if (!enemy.isAlive()){
		alert('Enemy is DEAD');
		return false;
	}
	this.addPoints(this.power);
	enemy.decreaseHealth(this.power);
	return true;
};

function Human() {
	Warrior.call(this, 800, 200);
}
Human.prototype = Object.create(Warrior.prototype);
function Ork() {
	Warrior.call(this, 2000, 250);
}
Ork.prototype = Object.create(Warrior.prototype);
function Elf(health = 500, power= 50) {
	Warrior.call(this, health,);
}
Elf.prototype = Object.create(Warrior.prototype);
function WhiteElf() {
	Elf.call(this, 300, 80);
}
WhiteElf.prototype = Object.create(Elf.prototype);
function BlackElf() {
	Elf.call(this, 900, 90);
}
BlackElf.prototype = Object.create(Elf.prototype);
