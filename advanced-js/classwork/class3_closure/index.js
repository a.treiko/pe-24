
// Task1 ==========

// Напишите функцию changeTextSize, у которой будут такие аргументы:

// Ссылка на DOM-элемент, размер текста которого нужно изменить без регистрации
// и sms.
// Величина в px, на которую нужно изменить текст, возвращает функцию, меняющую
// размер текста на заданную величину.

// С помощью этой функции создайте две:

// одна увеличивает текст на 2px от изначального;
// вторая - уменьшает на 3px.

// После чего повесьте полученнные функции в качестве обработчиков на кнопки с
// id="increase-text" и id="decrease-text". */

let btnDec = document.getElementById('decrease-text');
let btnInc = document.getElementById('increase-text');
let text = document.getElementById('text');
let size = text.style.fontSize;

let changeTextSize = function({style}, fontSize) {

    fontSize = parseInt(fontSize);

    return function(value) {
        style.fontSize = fontSize + value + 'px';
        fontSize += value;
    }
}

let fn = changeTextSize(text, text.style.fontSize);

btnInc.addEventListener('click', function(){
    fn(2)
});
btnDec.addEventListener('click', function(){
    fn(-3)
})



