
// ========== Task 3 ==========

// Напишите функцию, которая создает объект.
// В качестве аргументов она принимает в себя
// имя, фамилию и перечень строк формата "имяСвойства": значение .
// Их может быть много.

const createObject = (firstName, lastName, ...rest) => {
    let user = {firstName, lastName};
    
    for (const prop of rest) {
        // Деструктуризация массива:
        let [key, value] = prop.split(': ');
        user[key] = value;
    }
    return user
}
const user = createObject('Золар', 'Аперкаль', 'status: Глава Юного клана Мексии', 'wife: Иврейн');
console.log(user);

