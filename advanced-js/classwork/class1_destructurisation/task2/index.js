
// Task 2

// Напишите функцию getPatientsStatus, которрая принимает в качестве аргументов
// рост в сантиментрах и вес в кг пациента, и возвращает 2 параметра -
// индекс массы тела и степень ожирения.

// Степени ожирения при разном значении индекса массы тела (m / h * h):

// - от 10 до 15 - анорексия;
// - от 16 до 25 - норма;
// - от 26 до 30 - лишний вес;
// - от 31 до 35 - I степень;
// - от 36 до 40 - II степень;
// - от 41 и выше - III степень;

const getPatientsStatus = function(height, weight) {
    // Формула просчета индекса:
    let indexMass = weight / (height / 100) ** 2;
    // Массив данных
    let arr = [

        {anorexia: [10, 15]},
        {normal: [16, 25]},
        {overWeight: [26, 30]},
        {firstLevel: [31, 35]},
        {secondLevel: [36, 40]},
        {thirdLevel: [41, 999]},

    ];

    // Status - статус массы тела (anorexia, normal...)
    let status;
    // Перебираем массив с объектами внутри него:
    arr.forEach(item => {
        // Перебираем ключи и свойства каждого объекта в массиве (Object.entries - перегнали объект в массив):
        for (const [key, [min, max]] of Object.entries(item)) {
            if (indexMass <= max && indexMass >= min) {
                status = key;
            }
            // console.log(`${key}: ${min}, ${max}`);
        }
    });
    return [indexMass, status];
};

let patientStatus = getPatientsStatus(171, 84);
console.log(patientStatus); //: [ 28.72 , 'overWeight']

// Object.entries - метод возвращает массив перечисляемых свойств
// указанного объекта в формате [key, value], в том же порядке,
// что и for..in