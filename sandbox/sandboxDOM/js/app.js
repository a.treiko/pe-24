

for (let i = 0; i < 10; i++) {
    const div = document.createElement('div');
    div.textContent = i;
    const color = `rgb(
            ${Math.floor(Math.random() * 255)},
            ${Math.floor(Math.random() * 255)},
            ${Math.floor(Math.random() * 255)},)`;

        div.setAttribute('data-color', color);
        div.style.color = div.getAttribute('data-color');
        div.style.fontWeight = '700';
        div.style.fontSize = '30px'

    document.body.prepend(div);
}

const divs = document.querySelectorAll('div');

Array.from(divs).forEach((element) => {
    console.log(element.textContent);
});

Array.from(divs).forEach((element, index) => {
    element.className = `element_${index}`;
});

