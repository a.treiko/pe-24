

const root = $('#root');

console.log($);
console.log(root);

$('li').css({
    color: '#00f',
    'font-size': '28px',
});

$('li').each(function(index, element) {
    $(element).text(index);
});

const firstLi = $('li')[0];

$(firstLi).css('color', '#f0f');
$('li')[1].style.color = '#f0f';

$('li').attr('class', 'item');
// $($('li')[1]).attr('class', 'item1');

$($('li')[1]).addClass('item1');

console.log($($('li')));

$(document).on('click', 'li', (e) => {
    $(e.target).fadeOut(600);
});

// Задача #1 -------------------

$(document).on('click', '#btn', (e) => {
    $('ul').toggle('slow');
    $('#btn').toggleClass('closed');
});




