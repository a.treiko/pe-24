


let people = [
  {name: 'Artem', age: 28, budget: 20000},
  {name: 'Marcus', age: 38, budget: 30000},
  {name: 'Milla', age: 25, budget: 22000},
  {name: 'Antony', age: 17, budget: 18000},
  {name: 'Dominic', age: 35, budget: 34000},
  {name: 'Francis', age: 18, budget: 5000},
]

// for (let i = 0; i < people.length; i++) {
//   console.log(people[i]);
// }

// for (let person in people) {
//   console.log(person);
// }

// === FOREACH
// people.forEach(function(person){
//   // console.log(person);
//   // console.log(index);
//   // console.log(peopleArray);
// })

// people.forEach(person => console.log(person));

// === MAP применяет функцию и записует результат в новый массив
// let newPeople = people.map(person => {
//   return `${person.name} (${person.age})`;
// })
// console.log(newPeople);

// let peopleAgeMulti = people.map(person => person.age * 3);
// console.log(peopleAgeMulti);


// === Filter 
// const adults = [];
// for (let i = 0; i < people.length; i++) {
//   if (people[i].age > 18) {
//     adults.push(people[i]);
//   }
// }
// console.log(adults);

// const adults = people.filter(person =>{
//   if (person.age >= 18) {
//     return true;
//   }
// })
// console.log(adults);

// const adults = people.filter(person => person.age >= 18);
// console.log(adults);

// === REDUCE
// let amount = 0;
// for (let i = 0; i < people.length; i++) {
//   amount += people[i].budget;
// }
// console.log(amount);

// const amount = people.reduce((total, person ) => {
//   return total + person.budget;
// }, 0)
// console.log(amount);

// FIND 
// let milla = people.find(person => person.name === 'Milla')
// console.log(milla);

// FIND INDEX
// let millaIndex = people.findIndex(person => person.name === 'Milla')
// console.log(millaIndex);



const amount = people
      .filter(person => person.budget > 5000)
      .map(person => {
        return {
          info: `${person.name} (${person.age})`,
          budget: person.budget
        }
      })
      .reduce((total, person) => total + person.budget, 0)
console.log(amount);