
// Задание
// Создать элемент h1 с текстом «Добро пожаловать!».
// Под элементом h1 добавить элемент button c текстом «Раскрасить».
// При клике по кнопке менять цвет каждой буквы элемента h1 на случайный.
// /_ Дано _/
const PHRASE = 'Добро пожаловать!';
function getRandomColor() {
    const r = Math.floor(Math.random() * 255);
    const g = Math.floor(Math.random() * 255);
    const b = Math.floor(Math.random() * 255);

    return `rgb(${r}, ${g}, ${b})`;
}

// Улучшить скрипт из предыдущего задания.
// При каждом движении курсора по странице
// менять цвет каждой буквы элемента h1 на случайный.

let h1 = document.createElement('h1');
h1.textContent = 'Добро пожаловать';
document.body.append(h1);

let btn = document.createElement('button');
btn.textContent = 'Раскрыть';
document.body.append(btn);

btn.addEventListener('click', function(){
    // Разбиваем на массив строк - строку PHRASE
    // имея под каждым item строку в виде буквы
    // создаем спан для каджой разделенной буквы и записываем в новый массив, 
    // чтоб придать стили к каждой букве
    let newArr = PHRASE.split('').map((item) => {
        let span = document.createElement('span');
        span.textContent = item;
        span.style.color = getRandomColor();
        return span;
    })
    const fragment = document.createDocumentFragment();
    fragment.append(...newArr);
    // Иначе будем добавлять каждый раз новый заголовок с разноцветными буквами:
    h1.innerHTML = '';
    h1.append(fragment);
});