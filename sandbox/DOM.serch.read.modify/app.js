
// Напишите фунцию, которая будет перебирать все элементы списка
// Находить атрибут и добавлять в элемент списка картинку (обернутую в ссылку)
// При этом все картинки должны быть одного формата

// КОЛЛЕКЦИЯ li:
// const collection = document.querySelectorAll('li');

// collection.forEach((item) => {
    
//     let childAnchor = document.querySelector('a');
//     console.log(item.dateset);
//     const tempInfo = {
//         title: item.dataset.title,
//         img: item.dataset.img,
//         link: childAnchor.dataset.link,
//     };
//     childAnchor.setAttribute('href', tempInfo.link);
//     childAnchor.innerHTML = `<span>${tempInfo.title}</span> <img src="${tempInfo.img}"`;
// });

const collection = document.querySelector('li');

collection.forEach(item => {
    let childAnchor = item.querySelector('a');
    const tempInfo = {
        title: item.dataset.title,
        img: item.dataset.img,
        link: childAnchor.dateset.link,
    }
    childAnchor.setAttribute('href', tempInfo.link);
    childAnchor.innerHTML = `<span>${tempInfo.title}</span><img src="${tempInfo.img}">`;
});